import { IItensLocacao } from 'app/shared/model/itens-locacao.model';
import { IFilme } from 'app/shared/model/filme.model';

export const enum IdiomaFilme {
    PORTUGUES = 'PORTUGUES',
    ESPANHOL = 'ESPANHOL',
    INGLES = 'INGLES'
}

export const enum TipoMidia {
    DVD = 'DVD',
    CD = 'CD',
    VHS = 'VHS',
    BLU_RAY = 'BLU_RAY',
    DISCO_VINIL = 'DISCO_VINIL',
    K7 = 'K7'
}

export const enum StatusMidia {
    LOCADA = 'LOCADA',
    RESERVADA = 'RESERVADA',
    OUTROS = 'OUTROS'
}

export interface IMidia {
    id?: number;
    idioma?: IdiomaFilme;
    tipo?: TipoMidia;
    statusMidia?: StatusMidia;
    itensLocacaos?: IItensLocacao[];
    filmes?: IFilme[];
}

export class Midia implements IMidia {
    constructor(
        public id?: number,
        public idioma?: IdiomaFilme,
        public tipo?: TipoMidia,
        public statusMidia?: StatusMidia,
        public itensLocacaos?: IItensLocacao[],
        public filmes?: IFilme[]
    ) {}
}
