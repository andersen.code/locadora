export const enum TipoPessoa {
    FISICA = 'FISICA',
    JURIDICA = 'JURIDICA'
}

export const enum ClienteSituacao {
    ATIVO = 'ATIVO',
    INATIVO = 'INATIVO',
    BLOQUEADO = 'BLOQUEADO'
}

export interface IDadosPessoais {
    id?: number;
    tipoPessoa?: TipoPessoa;
    nome?: string;
    documentoReceitaFederal?: string;
    situacao?: ClienteSituacao;
    observacao?: string;
    userEmail?: string;
    userId?: number;
}

export class DadosPessoais implements IDadosPessoais {
    constructor(
        public id?: number,
        public tipoPessoa?: TipoPessoa,
        public nome?: string,
        public documentoReceitaFederal?: string,
        public situacao?: ClienteSituacao,
        public observacao?: string,
        public userEmail?: string,
        public userId?: number
    ) {}
}
