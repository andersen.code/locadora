import { IFilme } from 'app/shared/model/filme.model';

export interface IDiretor {
    id?: number;
    nome?: string;
    observacao?: string;
    filmes?: IFilme[];
}

export class Diretor implements IDiretor {
    constructor(public id?: number, public nome?: string, public observacao?: string, public filmes?: IFilme[]) {}
}
