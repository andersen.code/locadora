import { IMidia } from 'app/shared/model/midia.model';
import { IAtor } from 'app/shared/model/ator.model';

export const enum Categoria {
    COMEDIA = 'COMEDIA',
    ACAO = 'ACAO',
    AVENTURA = 'AVENTURA',
    ANIME = 'ANIME',
    FAMILIA = 'FAMILIA',
    KIDS = 'KIDS',
    CULT = 'CULT',
    DOCUMENTARIOS = 'DOCUMENTARIOS',
    DRAMAS = 'DRAMAS',
    TERROS = 'TERROS'
}

export interface IFilme {
    id?: number;
    titulo?: string;
    categoria?: Categoria;
    diretorNome?: string;
    diretorId?: number;
    midias?: IMidia[];
    ators?: IAtor[];
}

export class Filme implements IFilme {
    constructor(
        public id?: number,
        public titulo?: string,
        public categoria?: Categoria,
        public diretorNome?: string,
        public diretorId?: number,
        public midias?: IMidia[],
        public ators?: IAtor[]
    ) {}
}
