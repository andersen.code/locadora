import { Moment } from 'moment';

export interface IReserva {
    id?: number;
    dataLocacaoPrevista?: Moment;
    dataValidadeReserva?: Moment;
    observacao?: string;
    userEmail?: string;
    userId?: number;
}

export class Reserva implements IReserva {
    constructor(
        public id?: number,
        public dataLocacaoPrevista?: Moment,
        public dataValidadeReserva?: Moment,
        public observacao?: string,
        public userEmail?: string,
        public userId?: number
    ) {}
}
