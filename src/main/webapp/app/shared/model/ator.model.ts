import { IFilme } from 'app/shared/model/filme.model';

export interface IAtor {
    id?: number;
    nome?: string;
    filmes?: IFilme[];
}

export class Ator implements IAtor {
    constructor(public id?: number, public nome?: string, public filmes?: IFilme[]) {}
}
