import { Moment } from 'moment';

export interface IItensLocacao {
    id?: number;
    dataEntregaPrevista?: Moment;
    dataEntregaEfetiva?: Moment;
    midiaTipo?: string;
    midiaId?: number;
    locacaoId?: number;
}

export class ItensLocacao implements IItensLocacao {
    constructor(
        public id?: number,
        public dataEntregaPrevista?: Moment,
        public dataEntregaEfetiva?: Moment,
        public midiaTipo?: string,
        public midiaId?: number,
        public locacaoId?: number
    ) {}
}
