import { Moment } from 'moment';
import { IItensLocacao } from 'app/shared/model/itens-locacao.model';

export interface ILocacao {
    id?: number;
    dataLocacao?: Moment;
    valorLocacao?: number;
    observacao?: string;
    itensLocacaos?: IItensLocacao[];
    userEmail?: string;
    userId?: number;
}

export class Locacao implements ILocacao {
    constructor(
        public id?: number,
        public dataLocacao?: Moment,
        public valorLocacao?: number,
        public observacao?: string,
        public itensLocacaos?: IItensLocacao[],
        public userEmail?: string,
        public userId?: number
    ) {}
}
