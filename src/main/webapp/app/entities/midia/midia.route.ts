import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Midia } from 'app/shared/model/midia.model';
import { MidiaService } from './midia.service';
import { MidiaComponent } from './midia.component';
import { MidiaDetailComponent } from './midia-detail.component';
import { MidiaUpdateComponent } from './midia-update.component';
import { MidiaDeletePopupComponent } from './midia-delete-dialog.component';
import { IMidia } from 'app/shared/model/midia.model';

@Injectable({ providedIn: 'root' })
export class MidiaResolve implements Resolve<IMidia> {
    constructor(private service: MidiaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMidia> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Midia>) => response.ok),
                map((midia: HttpResponse<Midia>) => midia.body)
            );
        }
        return of(new Midia());
    }
}

export const midiaRoute: Routes = [
    {
        path: '',
        component: MidiaComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'locadoraApp.midia.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: MidiaDetailComponent,
        resolve: {
            midia: MidiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.midia.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: MidiaUpdateComponent,
        resolve: {
            midia: MidiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.midia.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: MidiaUpdateComponent,
        resolve: {
            midia: MidiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.midia.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const midiaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: MidiaDeletePopupComponent,
        resolve: {
            midia: MidiaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.midia.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
