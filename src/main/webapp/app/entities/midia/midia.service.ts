import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMidia } from 'app/shared/model/midia.model';

type EntityResponseType = HttpResponse<IMidia>;
type EntityArrayResponseType = HttpResponse<IMidia[]>;

@Injectable({ providedIn: 'root' })
export class MidiaService {
    public resourceUrl = SERVER_API_URL + 'api/midias';

    constructor(protected http: HttpClient) {}

    create(midia: IMidia): Observable<EntityResponseType> {
        return this.http.post<IMidia>(this.resourceUrl, midia, { observe: 'response' });
    }

    update(midia: IMidia): Observable<EntityResponseType> {
        return this.http.put<IMidia>(this.resourceUrl, midia, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IMidia>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IMidia[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
