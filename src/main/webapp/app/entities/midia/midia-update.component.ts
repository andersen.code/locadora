import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IMidia } from 'app/shared/model/midia.model';
import { MidiaService } from './midia.service';
import { IFilme } from 'app/shared/model/filme.model';
import { FilmeService } from 'app/entities/filme';

@Component({
    selector: 'jhi-midia-update',
    templateUrl: './midia-update.component.html'
})
export class MidiaUpdateComponent implements OnInit {
    midia: IMidia;
    isSaving: boolean;

    filmes: IFilme[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected midiaService: MidiaService,
        protected filmeService: FilmeService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ midia }) => {
            this.midia = midia;
        });
        this.filmeService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IFilme[]>) => mayBeOk.ok),
                map((response: HttpResponse<IFilme[]>) => response.body)
            )
            .subscribe((res: IFilme[]) => (this.filmes = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.midia.id !== undefined) {
            this.subscribeToSaveResponse(this.midiaService.update(this.midia));
        } else {
            this.subscribeToSaveResponse(this.midiaService.create(this.midia));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IMidia>>) {
        result.subscribe((res: HttpResponse<IMidia>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackFilmeById(index: number, item: IFilme) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
