import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMidia } from 'app/shared/model/midia.model';

@Component({
    selector: 'jhi-midia-detail',
    templateUrl: './midia-detail.component.html'
})
export class MidiaDetailComponent implements OnInit {
    midia: IMidia;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ midia }) => {
            this.midia = midia;
        });
    }

    previousState() {
        window.history.back();
    }
}
