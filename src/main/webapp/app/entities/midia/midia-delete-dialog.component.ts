import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMidia } from 'app/shared/model/midia.model';
import { MidiaService } from './midia.service';

@Component({
    selector: 'jhi-midia-delete-dialog',
    templateUrl: './midia-delete-dialog.component.html'
})
export class MidiaDeleteDialogComponent {
    midia: IMidia;

    constructor(protected midiaService: MidiaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.midiaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'midiaListModification',
                content: 'Deleted an midia'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-midia-delete-popup',
    template: ''
})
export class MidiaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ midia }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(MidiaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.midia = midia;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/midia', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/midia', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
