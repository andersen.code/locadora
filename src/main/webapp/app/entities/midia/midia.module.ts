import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { LocadoraSharedModule } from 'app/shared';
import {
    MidiaComponent,
    MidiaDetailComponent,
    MidiaUpdateComponent,
    MidiaDeletePopupComponent,
    MidiaDeleteDialogComponent,
    midiaRoute,
    midiaPopupRoute
} from './';

const ENTITY_STATES = [...midiaRoute, ...midiaPopupRoute];

@NgModule({
    imports: [LocadoraSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [MidiaComponent, MidiaDetailComponent, MidiaUpdateComponent, MidiaDeleteDialogComponent, MidiaDeletePopupComponent],
    entryComponents: [MidiaComponent, MidiaUpdateComponent, MidiaDeleteDialogComponent, MidiaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LocadoraMidiaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
