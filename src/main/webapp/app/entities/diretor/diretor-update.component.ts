import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IDiretor } from 'app/shared/model/diretor.model';
import { DiretorService } from './diretor.service';

@Component({
    selector: 'jhi-diretor-update',
    templateUrl: './diretor-update.component.html'
})
export class DiretorUpdateComponent implements OnInit {
    diretor: IDiretor;
    isSaving: boolean;

    constructor(protected diretorService: DiretorService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ diretor }) => {
            this.diretor = diretor;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.diretor.id !== undefined) {
            this.subscribeToSaveResponse(this.diretorService.update(this.diretor));
        } else {
            this.subscribeToSaveResponse(this.diretorService.create(this.diretor));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IDiretor>>) {
        result.subscribe((res: HttpResponse<IDiretor>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
