import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Diretor } from 'app/shared/model/diretor.model';
import { DiretorService } from './diretor.service';
import { DiretorComponent } from './diretor.component';
import { DiretorDetailComponent } from './diretor-detail.component';
import { DiretorUpdateComponent } from './diretor-update.component';
import { DiretorDeletePopupComponent } from './diretor-delete-dialog.component';
import { IDiretor } from 'app/shared/model/diretor.model';

@Injectable({ providedIn: 'root' })
export class DiretorResolve implements Resolve<IDiretor> {
    constructor(private service: DiretorService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDiretor> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Diretor>) => response.ok),
                map((diretor: HttpResponse<Diretor>) => diretor.body)
            );
        }
        return of(new Diretor());
    }
}

export const diretorRoute: Routes = [
    {
        path: '',
        component: DiretorComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'locadoraApp.diretor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: DiretorDetailComponent,
        resolve: {
            diretor: DiretorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.diretor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: DiretorUpdateComponent,
        resolve: {
            diretor: DiretorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.diretor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: DiretorUpdateComponent,
        resolve: {
            diretor: DiretorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.diretor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const diretorPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: DiretorDeletePopupComponent,
        resolve: {
            diretor: DiretorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.diretor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
