import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDiretor } from 'app/shared/model/diretor.model';

type EntityResponseType = HttpResponse<IDiretor>;
type EntityArrayResponseType = HttpResponse<IDiretor[]>;

@Injectable({ providedIn: 'root' })
export class DiretorService {
    public resourceUrl = SERVER_API_URL + 'api/diretors';

    constructor(protected http: HttpClient) {}

    create(diretor: IDiretor): Observable<EntityResponseType> {
        return this.http.post<IDiretor>(this.resourceUrl, diretor, { observe: 'response' });
    }

    update(diretor: IDiretor): Observable<EntityResponseType> {
        return this.http.put<IDiretor>(this.resourceUrl, diretor, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDiretor>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDiretor[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
