import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDiretor } from 'app/shared/model/diretor.model';
import { DiretorService } from './diretor.service';

@Component({
    selector: 'jhi-diretor-delete-dialog',
    templateUrl: './diretor-delete-dialog.component.html'
})
export class DiretorDeleteDialogComponent {
    diretor: IDiretor;

    constructor(protected diretorService: DiretorService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.diretorService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'diretorListModification',
                content: 'Deleted an diretor'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-diretor-delete-popup',
    template: ''
})
export class DiretorDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ diretor }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DiretorDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.diretor = diretor;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/diretor', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/diretor', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
