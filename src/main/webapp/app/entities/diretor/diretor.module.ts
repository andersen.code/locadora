import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { LocadoraSharedModule } from 'app/shared';
import {
    DiretorComponent,
    DiretorDetailComponent,
    DiretorUpdateComponent,
    DiretorDeletePopupComponent,
    DiretorDeleteDialogComponent,
    diretorRoute,
    diretorPopupRoute
} from './';

const ENTITY_STATES = [...diretorRoute, ...diretorPopupRoute];

@NgModule({
    imports: [LocadoraSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DiretorComponent,
        DiretorDetailComponent,
        DiretorUpdateComponent,
        DiretorDeleteDialogComponent,
        DiretorDeletePopupComponent
    ],
    entryComponents: [DiretorComponent, DiretorUpdateComponent, DiretorDeleteDialogComponent, DiretorDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LocadoraDiretorModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
