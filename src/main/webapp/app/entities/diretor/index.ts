export * from './diretor.service';
export * from './diretor-update.component';
export * from './diretor-delete-dialog.component';
export * from './diretor-detail.component';
export * from './diretor.component';
export * from './diretor.route';
