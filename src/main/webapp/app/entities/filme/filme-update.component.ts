import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IFilme } from 'app/shared/model/filme.model';
import { FilmeService } from './filme.service';
import { IDiretor } from 'app/shared/model/diretor.model';
import { DiretorService } from 'app/entities/diretor';
import { IMidia } from 'app/shared/model/midia.model';
import { MidiaService } from 'app/entities/midia';
import { IAtor } from 'app/shared/model/ator.model';
import { AtorService } from 'app/entities/ator';

@Component({
    selector: 'jhi-filme-update',
    templateUrl: './filme-update.component.html'
})
export class FilmeUpdateComponent implements OnInit {
    filme: IFilme;
    isSaving: boolean;

    diretors: IDiretor[];

    midias: IMidia[];

    ators: IAtor[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected filmeService: FilmeService,
        protected diretorService: DiretorService,
        protected midiaService: MidiaService,
        protected atorService: AtorService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ filme }) => {
            this.filme = filme;
        });
        this.diretorService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IDiretor[]>) => mayBeOk.ok),
                map((response: HttpResponse<IDiretor[]>) => response.body)
            )
            .subscribe((res: IDiretor[]) => (this.diretors = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.midiaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMidia[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMidia[]>) => response.body)
            )
            .subscribe((res: IMidia[]) => (this.midias = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.atorService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IAtor[]>) => mayBeOk.ok),
                map((response: HttpResponse<IAtor[]>) => response.body)
            )
            .subscribe((res: IAtor[]) => (this.ators = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.filme.id !== undefined) {
            this.subscribeToSaveResponse(this.filmeService.update(this.filme));
        } else {
            this.subscribeToSaveResponse(this.filmeService.create(this.filme));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IFilme>>) {
        result.subscribe((res: HttpResponse<IFilme>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDiretorById(index: number, item: IDiretor) {
        return item.id;
    }

    trackMidiaById(index: number, item: IMidia) {
        return item.id;
    }

    trackAtorById(index: number, item: IAtor) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
