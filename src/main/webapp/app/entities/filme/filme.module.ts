import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { LocadoraSharedModule } from 'app/shared';
import {
    FilmeComponent,
    FilmeDetailComponent,
    FilmeUpdateComponent,
    FilmeDeletePopupComponent,
    FilmeDeleteDialogComponent,
    filmeRoute,
    filmePopupRoute
} from './';

const ENTITY_STATES = [...filmeRoute, ...filmePopupRoute];

@NgModule({
    imports: [LocadoraSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [FilmeComponent, FilmeDetailComponent, FilmeUpdateComponent, FilmeDeleteDialogComponent, FilmeDeletePopupComponent],
    entryComponents: [FilmeComponent, FilmeUpdateComponent, FilmeDeleteDialogComponent, FilmeDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LocadoraFilmeModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
