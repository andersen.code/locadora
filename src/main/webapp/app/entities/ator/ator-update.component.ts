import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAtor } from 'app/shared/model/ator.model';
import { AtorService } from './ator.service';
import { IFilme } from 'app/shared/model/filme.model';
import { FilmeService } from 'app/entities/filme';

@Component({
    selector: 'jhi-ator-update',
    templateUrl: './ator-update.component.html'
})
export class AtorUpdateComponent implements OnInit {
    ator: IAtor;
    isSaving: boolean;

    filmes: IFilme[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected atorService: AtorService,
        protected filmeService: FilmeService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ ator }) => {
            this.ator = ator;
        });
        this.filmeService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IFilme[]>) => mayBeOk.ok),
                map((response: HttpResponse<IFilme[]>) => response.body)
            )
            .subscribe((res: IFilme[]) => (this.filmes = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.ator.id !== undefined) {
            this.subscribeToSaveResponse(this.atorService.update(this.ator));
        } else {
            this.subscribeToSaveResponse(this.atorService.create(this.ator));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAtor>>) {
        result.subscribe((res: HttpResponse<IAtor>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackFilmeById(index: number, item: IFilme) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
