import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAtor } from 'app/shared/model/ator.model';
import { AtorService } from './ator.service';

@Component({
    selector: 'jhi-ator-delete-dialog',
    templateUrl: './ator-delete-dialog.component.html'
})
export class AtorDeleteDialogComponent {
    ator: IAtor;

    constructor(protected atorService: AtorService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.atorService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'atorListModification',
                content: 'Deleted an ator'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ator-delete-popup',
    template: ''
})
export class AtorDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ator }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AtorDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.ator = ator;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/ator', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/ator', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
