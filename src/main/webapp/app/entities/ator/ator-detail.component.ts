import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAtor } from 'app/shared/model/ator.model';

@Component({
    selector: 'jhi-ator-detail',
    templateUrl: './ator-detail.component.html'
})
export class AtorDetailComponent implements OnInit {
    ator: IAtor;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ator }) => {
            this.ator = ator;
        });
    }

    previousState() {
        window.history.back();
    }
}
