import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { LocadoraSharedModule } from 'app/shared';
import {
    AtorComponent,
    AtorDetailComponent,
    AtorUpdateComponent,
    AtorDeletePopupComponent,
    AtorDeleteDialogComponent,
    atorRoute,
    atorPopupRoute
} from './';

const ENTITY_STATES = [...atorRoute, ...atorPopupRoute];

@NgModule({
    imports: [LocadoraSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [AtorComponent, AtorDetailComponent, AtorUpdateComponent, AtorDeleteDialogComponent, AtorDeletePopupComponent],
    entryComponents: [AtorComponent, AtorUpdateComponent, AtorDeleteDialogComponent, AtorDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LocadoraAtorModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
