export * from './ator.service';
export * from './ator-update.component';
export * from './ator-delete-dialog.component';
export * from './ator-detail.component';
export * from './ator.component';
export * from './ator.route';
