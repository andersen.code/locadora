import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Ator } from 'app/shared/model/ator.model';
import { AtorService } from './ator.service';
import { AtorComponent } from './ator.component';
import { AtorDetailComponent } from './ator-detail.component';
import { AtorUpdateComponent } from './ator-update.component';
import { AtorDeletePopupComponent } from './ator-delete-dialog.component';
import { IAtor } from 'app/shared/model/ator.model';

@Injectable({ providedIn: 'root' })
export class AtorResolve implements Resolve<IAtor> {
    constructor(private service: AtorService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAtor> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Ator>) => response.ok),
                map((ator: HttpResponse<Ator>) => ator.body)
            );
        }
        return of(new Ator());
    }
}

export const atorRoute: Routes = [
    {
        path: '',
        component: AtorComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'locadoraApp.ator.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: AtorDetailComponent,
        resolve: {
            ator: AtorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.ator.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: AtorUpdateComponent,
        resolve: {
            ator: AtorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.ator.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: AtorUpdateComponent,
        resolve: {
            ator: AtorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.ator.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const atorPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: AtorDeletePopupComponent,
        resolve: {
            ator: AtorResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.ator.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
