import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAtor } from 'app/shared/model/ator.model';

type EntityResponseType = HttpResponse<IAtor>;
type EntityArrayResponseType = HttpResponse<IAtor[]>;

@Injectable({ providedIn: 'root' })
export class AtorService {
    public resourceUrl = SERVER_API_URL + 'api/ators';

    constructor(protected http: HttpClient) {}

    create(ator: IAtor): Observable<EntityResponseType> {
        return this.http.post<IAtor>(this.resourceUrl, ator, { observe: 'response' });
    }

    update(ator: IAtor): Observable<EntityResponseType> {
        return this.http.put<IAtor>(this.resourceUrl, ator, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAtor>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAtor[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
