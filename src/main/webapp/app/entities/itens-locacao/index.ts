export * from './itens-locacao.service';
export * from './itens-locacao-update.component';
export * from './itens-locacao-delete-dialog.component';
export * from './itens-locacao-detail.component';
export * from './itens-locacao.component';
export * from './itens-locacao.route';
