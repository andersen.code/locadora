import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IItensLocacao } from 'app/shared/model/itens-locacao.model';

@Component({
    selector: 'jhi-itens-locacao-detail',
    templateUrl: './itens-locacao-detail.component.html'
})
export class ItensLocacaoDetailComponent implements OnInit {
    itensLocacao: IItensLocacao;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ itensLocacao }) => {
            this.itensLocacao = itensLocacao;
        });
    }

    previousState() {
        window.history.back();
    }
}
