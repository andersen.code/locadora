import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IItensLocacao } from 'app/shared/model/itens-locacao.model';
import { ItensLocacaoService } from './itens-locacao.service';
import { IMidia } from 'app/shared/model/midia.model';
import { MidiaService } from 'app/entities/midia';
import { ILocacao } from 'app/shared/model/locacao.model';
import { LocacaoService } from 'app/entities/locacao';

@Component({
    selector: 'jhi-itens-locacao-update',
    templateUrl: './itens-locacao-update.component.html'
})
export class ItensLocacaoUpdateComponent implements OnInit {
    itensLocacao: IItensLocacao;
    isSaving: boolean;

    midias: IMidia[];

    locacaos: ILocacao[];
    dataEntregaPrevistaDp: any;
    dataEntregaEfetivaDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected itensLocacaoService: ItensLocacaoService,
        protected midiaService: MidiaService,
        protected locacaoService: LocacaoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ itensLocacao }) => {
            this.itensLocacao = itensLocacao;
        });
        this.midiaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMidia[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMidia[]>) => response.body)
            )
            .subscribe((res: IMidia[]) => (this.midias = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.locacaoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ILocacao[]>) => mayBeOk.ok),
                map((response: HttpResponse<ILocacao[]>) => response.body)
            )
            .subscribe((res: ILocacao[]) => (this.locacaos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.itensLocacao.id !== undefined) {
            this.subscribeToSaveResponse(this.itensLocacaoService.update(this.itensLocacao));
        } else {
            this.subscribeToSaveResponse(this.itensLocacaoService.create(this.itensLocacao));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IItensLocacao>>) {
        result.subscribe((res: HttpResponse<IItensLocacao>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackMidiaById(index: number, item: IMidia) {
        return item.id;
    }

    trackLocacaoById(index: number, item: ILocacao) {
        return item.id;
    }
}
