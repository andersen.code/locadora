import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IItensLocacao } from 'app/shared/model/itens-locacao.model';

type EntityResponseType = HttpResponse<IItensLocacao>;
type EntityArrayResponseType = HttpResponse<IItensLocacao[]>;

@Injectable({ providedIn: 'root' })
export class ItensLocacaoService {
    public resourceUrl = SERVER_API_URL + 'api/itens-locacaos';

    constructor(protected http: HttpClient) {}

    create(itensLocacao: IItensLocacao): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(itensLocacao);
        return this.http
            .post<IItensLocacao>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(itensLocacao: IItensLocacao): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(itensLocacao);
        return this.http
            .put<IItensLocacao>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IItensLocacao>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IItensLocacao[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(itensLocacao: IItensLocacao): IItensLocacao {
        const copy: IItensLocacao = Object.assign({}, itensLocacao, {
            dataEntregaPrevista:
                itensLocacao.dataEntregaPrevista != null && itensLocacao.dataEntregaPrevista.isValid()
                    ? itensLocacao.dataEntregaPrevista.format(DATE_FORMAT)
                    : null,
            dataEntregaEfetiva:
                itensLocacao.dataEntregaEfetiva != null && itensLocacao.dataEntregaEfetiva.isValid()
                    ? itensLocacao.dataEntregaEfetiva.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.dataEntregaPrevista = res.body.dataEntregaPrevista != null ? moment(res.body.dataEntregaPrevista) : null;
            res.body.dataEntregaEfetiva = res.body.dataEntregaEfetiva != null ? moment(res.body.dataEntregaEfetiva) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((itensLocacao: IItensLocacao) => {
                itensLocacao.dataEntregaPrevista =
                    itensLocacao.dataEntregaPrevista != null ? moment(itensLocacao.dataEntregaPrevista) : null;
                itensLocacao.dataEntregaEfetiva = itensLocacao.dataEntregaEfetiva != null ? moment(itensLocacao.dataEntregaEfetiva) : null;
            });
        }
        return res;
    }
}
