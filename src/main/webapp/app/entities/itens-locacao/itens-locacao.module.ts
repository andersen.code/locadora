import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { LocadoraSharedModule } from 'app/shared';
import {
    ItensLocacaoComponent,
    ItensLocacaoDetailComponent,
    ItensLocacaoUpdateComponent,
    ItensLocacaoDeletePopupComponent,
    ItensLocacaoDeleteDialogComponent,
    itensLocacaoRoute,
    itensLocacaoPopupRoute
} from './';

const ENTITY_STATES = [...itensLocacaoRoute, ...itensLocacaoPopupRoute];

@NgModule({
    imports: [LocadoraSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ItensLocacaoComponent,
        ItensLocacaoDetailComponent,
        ItensLocacaoUpdateComponent,
        ItensLocacaoDeleteDialogComponent,
        ItensLocacaoDeletePopupComponent
    ],
    entryComponents: [
        ItensLocacaoComponent,
        ItensLocacaoUpdateComponent,
        ItensLocacaoDeleteDialogComponent,
        ItensLocacaoDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LocadoraItensLocacaoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
