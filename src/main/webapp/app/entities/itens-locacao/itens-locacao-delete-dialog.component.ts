import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IItensLocacao } from 'app/shared/model/itens-locacao.model';
import { ItensLocacaoService } from './itens-locacao.service';

@Component({
    selector: 'jhi-itens-locacao-delete-dialog',
    templateUrl: './itens-locacao-delete-dialog.component.html'
})
export class ItensLocacaoDeleteDialogComponent {
    itensLocacao: IItensLocacao;

    constructor(
        protected itensLocacaoService: ItensLocacaoService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.itensLocacaoService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'itensLocacaoListModification',
                content: 'Deleted an itensLocacao'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-itens-locacao-delete-popup',
    template: ''
})
export class ItensLocacaoDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ itensLocacao }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ItensLocacaoDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.itensLocacao = itensLocacao;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/itens-locacao', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/itens-locacao', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
