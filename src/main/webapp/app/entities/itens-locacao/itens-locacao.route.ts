import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ItensLocacao } from 'app/shared/model/itens-locacao.model';
import { ItensLocacaoService } from './itens-locacao.service';
import { ItensLocacaoComponent } from './itens-locacao.component';
import { ItensLocacaoDetailComponent } from './itens-locacao-detail.component';
import { ItensLocacaoUpdateComponent } from './itens-locacao-update.component';
import { ItensLocacaoDeletePopupComponent } from './itens-locacao-delete-dialog.component';
import { IItensLocacao } from 'app/shared/model/itens-locacao.model';

@Injectable({ providedIn: 'root' })
export class ItensLocacaoResolve implements Resolve<IItensLocacao> {
    constructor(private service: ItensLocacaoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IItensLocacao> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<ItensLocacao>) => response.ok),
                map((itensLocacao: HttpResponse<ItensLocacao>) => itensLocacao.body)
            );
        }
        return of(new ItensLocacao());
    }
}

export const itensLocacaoRoute: Routes = [
    {
        path: '',
        component: ItensLocacaoComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'locadoraApp.itensLocacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ItensLocacaoDetailComponent,
        resolve: {
            itensLocacao: ItensLocacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.itensLocacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ItensLocacaoUpdateComponent,
        resolve: {
            itensLocacao: ItensLocacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.itensLocacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ItensLocacaoUpdateComponent,
        resolve: {
            itensLocacao: ItensLocacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.itensLocacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const itensLocacaoPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ItensLocacaoDeletePopupComponent,
        resolve: {
            itensLocacao: ItensLocacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.itensLocacao.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
