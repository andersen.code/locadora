import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { LocadoraSharedModule } from 'app/shared';
import {
    DadosPessoaisComponent,
    DadosPessoaisDetailComponent,
    DadosPessoaisUpdateComponent,
    DadosPessoaisDeletePopupComponent,
    DadosPessoaisDeleteDialogComponent,
    dadosPessoaisRoute,
    dadosPessoaisPopupRoute
} from './';

const ENTITY_STATES = [...dadosPessoaisRoute, ...dadosPessoaisPopupRoute];

@NgModule({
    imports: [LocadoraSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DadosPessoaisComponent,
        DadosPessoaisDetailComponent,
        DadosPessoaisUpdateComponent,
        DadosPessoaisDeleteDialogComponent,
        DadosPessoaisDeletePopupComponent
    ],
    entryComponents: [
        DadosPessoaisComponent,
        DadosPessoaisUpdateComponent,
        DadosPessoaisDeleteDialogComponent,
        DadosPessoaisDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LocadoraDadosPessoaisModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
