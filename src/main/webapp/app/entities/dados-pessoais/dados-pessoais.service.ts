import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDadosPessoais } from 'app/shared/model/dados-pessoais.model';

type EntityResponseType = HttpResponse<IDadosPessoais>;
type EntityArrayResponseType = HttpResponse<IDadosPessoais[]>;

@Injectable({ providedIn: 'root' })
export class DadosPessoaisService {
    public resourceUrl = SERVER_API_URL + 'api/dados-pessoais';

    constructor(protected http: HttpClient) {}

    create(dadosPessoais: IDadosPessoais): Observable<EntityResponseType> {
        return this.http.post<IDadosPessoais>(this.resourceUrl, dadosPessoais, { observe: 'response' });
    }

    update(dadosPessoais: IDadosPessoais): Observable<EntityResponseType> {
        return this.http.put<IDadosPessoais>(this.resourceUrl, dadosPessoais, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDadosPessoais>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDadosPessoais[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
