import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DadosPessoais } from 'app/shared/model/dados-pessoais.model';
import { DadosPessoaisService } from './dados-pessoais.service';
import { DadosPessoaisComponent } from './dados-pessoais.component';
import { DadosPessoaisDetailComponent } from './dados-pessoais-detail.component';
import { DadosPessoaisUpdateComponent } from './dados-pessoais-update.component';
import { DadosPessoaisDeletePopupComponent } from './dados-pessoais-delete-dialog.component';
import { IDadosPessoais } from 'app/shared/model/dados-pessoais.model';

@Injectable({ providedIn: 'root' })
export class DadosPessoaisResolve implements Resolve<IDadosPessoais> {
    constructor(private service: DadosPessoaisService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDadosPessoais> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<DadosPessoais>) => response.ok),
                map((dadosPessoais: HttpResponse<DadosPessoais>) => dadosPessoais.body)
            );
        }
        return of(new DadosPessoais());
    }
}

export const dadosPessoaisRoute: Routes = [
    {
        path: '',
        component: DadosPessoaisComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'locadoraApp.dadosPessoais.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: DadosPessoaisDetailComponent,
        resolve: {
            dadosPessoais: DadosPessoaisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.dadosPessoais.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: DadosPessoaisUpdateComponent,
        resolve: {
            dadosPessoais: DadosPessoaisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.dadosPessoais.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: DadosPessoaisUpdateComponent,
        resolve: {
            dadosPessoais: DadosPessoaisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.dadosPessoais.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dadosPessoaisPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: DadosPessoaisDeletePopupComponent,
        resolve: {
            dadosPessoais: DadosPessoaisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'locadoraApp.dadosPessoais.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
