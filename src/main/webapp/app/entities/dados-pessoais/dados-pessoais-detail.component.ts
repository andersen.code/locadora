import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDadosPessoais } from 'app/shared/model/dados-pessoais.model';

@Component({
    selector: 'jhi-dados-pessoais-detail',
    templateUrl: './dados-pessoais-detail.component.html'
})
export class DadosPessoaisDetailComponent implements OnInit {
    dadosPessoais: IDadosPessoais;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dadosPessoais }) => {
            this.dadosPessoais = dadosPessoais;
        });
    }

    previousState() {
        window.history.back();
    }
}
