export * from './dados-pessoais.service';
export * from './dados-pessoais-update.component';
export * from './dados-pessoais-delete-dialog.component';
export * from './dados-pessoais-detail.component';
export * from './dados-pessoais.component';
export * from './dados-pessoais.route';
