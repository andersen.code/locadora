import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDadosPessoais } from 'app/shared/model/dados-pessoais.model';
import { DadosPessoaisService } from './dados-pessoais.service';

@Component({
    selector: 'jhi-dados-pessoais-delete-dialog',
    templateUrl: './dados-pessoais-delete-dialog.component.html'
})
export class DadosPessoaisDeleteDialogComponent {
    dadosPessoais: IDadosPessoais;

    constructor(
        protected dadosPessoaisService: DadosPessoaisService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dadosPessoaisService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'dadosPessoaisListModification',
                content: 'Deleted an dadosPessoais'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dados-pessoais-delete-popup',
    template: ''
})
export class DadosPessoaisDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dadosPessoais }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DadosPessoaisDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.dadosPessoais = dadosPessoais;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/dados-pessoais', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/dados-pessoais', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
