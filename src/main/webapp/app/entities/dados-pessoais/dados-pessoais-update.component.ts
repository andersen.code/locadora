import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IDadosPessoais } from 'app/shared/model/dados-pessoais.model';
import { DadosPessoaisService } from './dados-pessoais.service';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-dados-pessoais-update',
    templateUrl: './dados-pessoais-update.component.html'
})
export class DadosPessoaisUpdateComponent implements OnInit {
    dadosPessoais: IDadosPessoais;
    isSaving: boolean;

    users: IUser[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected dadosPessoaisService: DadosPessoaisService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ dadosPessoais }) => {
            this.dadosPessoais = dadosPessoais;
        });
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.dadosPessoais.id !== undefined) {
            this.subscribeToSaveResponse(this.dadosPessoaisService.update(this.dadosPessoais));
        } else {
            this.subscribeToSaveResponse(this.dadosPessoaisService.create(this.dadosPessoais));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IDadosPessoais>>) {
        result.subscribe((res: HttpResponse<IDadosPessoais>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }
}
