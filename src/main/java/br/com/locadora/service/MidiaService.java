package br.com.locadora.service;

import br.com.locadora.service.dto.MidiaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Midia.
 */
public interface MidiaService {

    /**
     * Save a midia.
     *
     * @param midiaDTO the entity to save
     * @return the persisted entity
     */
    MidiaDTO save(MidiaDTO midiaDTO);

    /**
     * Get all the midias.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<MidiaDTO> findAll(Pageable pageable);

    /**
     * Get all the Midia with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    Page<MidiaDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" midia.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<MidiaDTO> findOne(Long id);

    /**
     * Delete the "id" midia.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
