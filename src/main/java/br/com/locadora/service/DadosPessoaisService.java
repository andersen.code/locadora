package br.com.locadora.service;

import br.com.locadora.service.dto.DadosPessoaisDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing DadosPessoais.
 */
public interface DadosPessoaisService {

    /**
     * Save a dadosPessoais.
     *
     * @param dadosPessoaisDTO the entity to save
     * @return the persisted entity
     */
    DadosPessoaisDTO save(DadosPessoaisDTO dadosPessoaisDTO);

    /**
     * Get all the dadosPessoais.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DadosPessoaisDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dadosPessoais.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DadosPessoaisDTO> findOne(Long id);

    /**
     * Delete the "id" dadosPessoais.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
