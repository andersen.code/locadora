package br.com.locadora.service;

import br.com.locadora.service.dto.DiretorDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Diretor.
 */
public interface DiretorService {

    /**
     * Save a diretor.
     *
     * @param diretorDTO the entity to save
     * @return the persisted entity
     */
    DiretorDTO save(DiretorDTO diretorDTO);

    /**
     * Get all the diretors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DiretorDTO> findAll(Pageable pageable);


    /**
     * Get the "id" diretor.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DiretorDTO> findOne(Long id);

    /**
     * Delete the "id" diretor.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
