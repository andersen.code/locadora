package br.com.locadora.service.impl;

import br.com.locadora.service.AtorService;
import br.com.locadora.domain.Ator;
import br.com.locadora.repository.AtorRepository;
import br.com.locadora.service.dto.AtorDTO;
import br.com.locadora.service.mapper.AtorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Ator.
 */
@Service
@Transactional
public class AtorServiceImpl implements AtorService {

    private final Logger log = LoggerFactory.getLogger(AtorServiceImpl.class);

    private final AtorRepository atorRepository;

    private final AtorMapper atorMapper;

    public AtorServiceImpl(AtorRepository atorRepository, AtorMapper atorMapper) {
        this.atorRepository = atorRepository;
        this.atorMapper = atorMapper;
    }

    /**
     * Save a ator.
     *
     * @param atorDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AtorDTO save(AtorDTO atorDTO) {
        log.debug("Request to save Ator : {}", atorDTO);
        Ator ator = atorMapper.toEntity(atorDTO);
        ator = atorRepository.save(ator);
        return atorMapper.toDto(ator);
    }

    /**
     * Get all the ators.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AtorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Ators");
        return atorRepository.findAll(pageable)
            .map(atorMapper::toDto);
    }

    /**
     * Get all the Ator with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<AtorDTO> findAllWithEagerRelationships(Pageable pageable) {
        return atorRepository.findAllWithEagerRelationships(pageable).map(atorMapper::toDto);
    }
    

    /**
     * Get one ator by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AtorDTO> findOne(Long id) {
        log.debug("Request to get Ator : {}", id);
        return atorRepository.findOneWithEagerRelationships(id)
            .map(atorMapper::toDto);
    }

    /**
     * Delete the ator by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Ator : {}", id);
        atorRepository.deleteById(id);
    }
}
