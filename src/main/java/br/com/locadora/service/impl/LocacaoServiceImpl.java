package br.com.locadora.service.impl;

import br.com.locadora.service.LocacaoService;
import br.com.locadora.domain.Locacao;
import br.com.locadora.repository.LocacaoRepository;
import br.com.locadora.service.dto.LocacaoDTO;
import br.com.locadora.service.mapper.LocacaoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Locacao.
 */
@Service
@Transactional
public class LocacaoServiceImpl implements LocacaoService {

    private final Logger log = LoggerFactory.getLogger(LocacaoServiceImpl.class);

    private final LocacaoRepository locacaoRepository;

    private final LocacaoMapper locacaoMapper;

    public LocacaoServiceImpl(LocacaoRepository locacaoRepository, LocacaoMapper locacaoMapper) {
        this.locacaoRepository = locacaoRepository;
        this.locacaoMapper = locacaoMapper;
    }

    /**
     * Save a locacao.
     *
     * @param locacaoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LocacaoDTO save(LocacaoDTO locacaoDTO) {
        log.debug("Request to save Locacao : {}", locacaoDTO);
        Locacao locacao = locacaoMapper.toEntity(locacaoDTO);
        locacao = locacaoRepository.save(locacao);
        return locacaoMapper.toDto(locacao);
    }

    /**
     * Get all the locacaos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LocacaoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Locacaos");
        return locacaoRepository.findAll(pageable)
            .map(locacaoMapper::toDto);
    }


    /**
     * Get one locacao by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LocacaoDTO> findOne(Long id) {
        log.debug("Request to get Locacao : {}", id);
        return locacaoRepository.findById(id)
            .map(locacaoMapper::toDto);
    }

    /**
     * Delete the locacao by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Locacao : {}", id);
        locacaoRepository.deleteById(id);
    }
}
