package br.com.locadora.service.impl;

import br.com.locadora.service.ItensLocacaoService;
import br.com.locadora.domain.ItensLocacao;
import br.com.locadora.repository.ItensLocacaoRepository;
import br.com.locadora.service.dto.ItensLocacaoDTO;
import br.com.locadora.service.mapper.ItensLocacaoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing ItensLocacao.
 */
@Service
@Transactional
public class ItensLocacaoServiceImpl implements ItensLocacaoService {

    private final Logger log = LoggerFactory.getLogger(ItensLocacaoServiceImpl.class);

    private final ItensLocacaoRepository itensLocacaoRepository;

    private final ItensLocacaoMapper itensLocacaoMapper;

    public ItensLocacaoServiceImpl(ItensLocacaoRepository itensLocacaoRepository, ItensLocacaoMapper itensLocacaoMapper) {
        this.itensLocacaoRepository = itensLocacaoRepository;
        this.itensLocacaoMapper = itensLocacaoMapper;
    }

    /**
     * Save a itensLocacao.
     *
     * @param itensLocacaoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ItensLocacaoDTO save(ItensLocacaoDTO itensLocacaoDTO) {
        log.debug("Request to save ItensLocacao : {}", itensLocacaoDTO);
        ItensLocacao itensLocacao = itensLocacaoMapper.toEntity(itensLocacaoDTO);
        itensLocacao = itensLocacaoRepository.save(itensLocacao);
        return itensLocacaoMapper.toDto(itensLocacao);
    }

    /**
     * Get all the itensLocacaos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ItensLocacaoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ItensLocacaos");
        return itensLocacaoRepository.findAll(pageable)
            .map(itensLocacaoMapper::toDto);
    }


    /**
     * Get one itensLocacao by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ItensLocacaoDTO> findOne(Long id) {
        log.debug("Request to get ItensLocacao : {}", id);
        return itensLocacaoRepository.findById(id)
            .map(itensLocacaoMapper::toDto);
    }

    /**
     * Delete the itensLocacao by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ItensLocacao : {}", id);
        itensLocacaoRepository.deleteById(id);
    }
}
