package br.com.locadora.service.impl;

import br.com.locadora.service.DiretorService;
import br.com.locadora.domain.Diretor;
import br.com.locadora.repository.DiretorRepository;
import br.com.locadora.service.dto.DiretorDTO;
import br.com.locadora.service.mapper.DiretorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Diretor.
 */
@Service
@Transactional
public class DiretorServiceImpl implements DiretorService {

    private final Logger log = LoggerFactory.getLogger(DiretorServiceImpl.class);

    private final DiretorRepository diretorRepository;

    private final DiretorMapper diretorMapper;

    public DiretorServiceImpl(DiretorRepository diretorRepository, DiretorMapper diretorMapper) {
        this.diretorRepository = diretorRepository;
        this.diretorMapper = diretorMapper;
    }

    /**
     * Save a diretor.
     *
     * @param diretorDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DiretorDTO save(DiretorDTO diretorDTO) {
        log.debug("Request to save Diretor : {}", diretorDTO);
        Diretor diretor = diretorMapper.toEntity(diretorDTO);
        diretor = diretorRepository.save(diretor);
        return diretorMapper.toDto(diretor);
    }

    /**
     * Get all the diretors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DiretorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Diretors");
        return diretorRepository.findAll(pageable)
            .map(diretorMapper::toDto);
    }


    /**
     * Get one diretor by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DiretorDTO> findOne(Long id) {
        log.debug("Request to get Diretor : {}", id);
        return diretorRepository.findById(id)
            .map(diretorMapper::toDto);
    }

    /**
     * Delete the diretor by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Diretor : {}", id);
        diretorRepository.deleteById(id);
    }
}
