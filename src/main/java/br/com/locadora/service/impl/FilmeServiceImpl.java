package br.com.locadora.service.impl;

import br.com.locadora.service.FilmeService;
import br.com.locadora.domain.Filme;
import br.com.locadora.repository.FilmeRepository;
import br.com.locadora.service.dto.FilmeDTO;
import br.com.locadora.service.mapper.FilmeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Filme.
 */
@Service
@Transactional
public class FilmeServiceImpl implements FilmeService {

    private final Logger log = LoggerFactory.getLogger(FilmeServiceImpl.class);

    private final FilmeRepository filmeRepository;

    private final FilmeMapper filmeMapper;

    public FilmeServiceImpl(FilmeRepository filmeRepository, FilmeMapper filmeMapper) {
        this.filmeRepository = filmeRepository;
        this.filmeMapper = filmeMapper;
    }

    /**
     * Save a filme.
     *
     * @param filmeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public FilmeDTO save(FilmeDTO filmeDTO) {
        log.debug("Request to save Filme : {}", filmeDTO);
        Filme filme = filmeMapper.toEntity(filmeDTO);
        filme = filmeRepository.save(filme);
        return filmeMapper.toDto(filme);
    }

    /**
     * Get all the filmes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FilmeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Filmes");
        return filmeRepository.findAll(pageable)
            .map(filmeMapper::toDto);
    }

    @Override
    public Page<FilmeDTO> findAllFilmesDisponivel(Pageable pageable) {
        log.debug("Request to get all Disponiveis");
        return filmeRepository.findAll(pageable)
            .map(filmeMapper::toDto);
    }


    /**
     * Get one filme by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FilmeDTO> findOne(Long id) {
        log.debug("Request to get Filme : {}", id);
        return filmeRepository.findById(id)
            .map(filmeMapper::toDto);
    }

    /**
     * Delete the filme by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Filme : {}", id);
        filmeRepository.deleteById(id);
    }
}
