package br.com.locadora.service.impl;

import br.com.locadora.service.DadosPessoaisService;
import br.com.locadora.domain.DadosPessoais;
import br.com.locadora.repository.DadosPessoaisRepository;
import br.com.locadora.service.dto.DadosPessoaisDTO;
import br.com.locadora.service.mapper.DadosPessoaisMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing DadosPessoais.
 */
@Service
@Transactional
public class DadosPessoaisServiceImpl implements DadosPessoaisService {

    private final Logger log = LoggerFactory.getLogger(DadosPessoaisServiceImpl.class);

    private final DadosPessoaisRepository dadosPessoaisRepository;

    private final DadosPessoaisMapper dadosPessoaisMapper;

    public DadosPessoaisServiceImpl(DadosPessoaisRepository dadosPessoaisRepository, DadosPessoaisMapper dadosPessoaisMapper) {
        this.dadosPessoaisRepository = dadosPessoaisRepository;
        this.dadosPessoaisMapper = dadosPessoaisMapper;
    }

    /**
     * Save a dadosPessoais.
     *
     * @param dadosPessoaisDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DadosPessoaisDTO save(DadosPessoaisDTO dadosPessoaisDTO) {
        log.debug("Request to save DadosPessoais : {}", dadosPessoaisDTO);
        DadosPessoais dadosPessoais = dadosPessoaisMapper.toEntity(dadosPessoaisDTO);
        dadosPessoais = dadosPessoaisRepository.save(dadosPessoais);
        return dadosPessoaisMapper.toDto(dadosPessoais);
    }

    /**
     * Get all the dadosPessoais.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DadosPessoaisDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DadosPessoais");
        return dadosPessoaisRepository.findAll(pageable)
            .map(dadosPessoaisMapper::toDto);
    }


    /**
     * Get one dadosPessoais by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DadosPessoaisDTO> findOne(Long id) {
        log.debug("Request to get DadosPessoais : {}", id);
        return dadosPessoaisRepository.findById(id)
            .map(dadosPessoaisMapper::toDto);
    }

    /**
     * Delete the dadosPessoais by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DadosPessoais : {}", id);
        dadosPessoaisRepository.deleteById(id);
    }
}
