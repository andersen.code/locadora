package br.com.locadora.service.impl;

import br.com.locadora.service.MidiaService;
import br.com.locadora.domain.Midia;
import br.com.locadora.repository.MidiaRepository;
import br.com.locadora.service.dto.MidiaDTO;
import br.com.locadora.service.mapper.MidiaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Midia.
 */
@Service
@Transactional
public class MidiaServiceImpl implements MidiaService {

    private final Logger log = LoggerFactory.getLogger(MidiaServiceImpl.class);

    private final MidiaRepository midiaRepository;

    private final MidiaMapper midiaMapper;

    public MidiaServiceImpl(MidiaRepository midiaRepository, MidiaMapper midiaMapper) {
        this.midiaRepository = midiaRepository;
        this.midiaMapper = midiaMapper;
    }

    /**
     * Save a midia.
     *
     * @param midiaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public MidiaDTO save(MidiaDTO midiaDTO) {
        log.debug("Request to save Midia : {}", midiaDTO);
        Midia midia = midiaMapper.toEntity(midiaDTO);
        midia = midiaRepository.save(midia);
        return midiaMapper.toDto(midia);
    }

    /**
     * Get all the midias.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MidiaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Midias");
        return midiaRepository.findAll(pageable)
            .map(midiaMapper::toDto);
    }

    /**
     * Get all the Midia with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<MidiaDTO> findAllWithEagerRelationships(Pageable pageable) {
        return midiaRepository.findAllWithEagerRelationships(pageable).map(midiaMapper::toDto);
    }
    

    /**
     * Get one midia by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MidiaDTO> findOne(Long id) {
        log.debug("Request to get Midia : {}", id);
        return midiaRepository.findOneWithEagerRelationships(id)
            .map(midiaMapper::toDto);
    }

    /**
     * Delete the midia by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Midia : {}", id);
        midiaRepository.deleteById(id);
    }
}
