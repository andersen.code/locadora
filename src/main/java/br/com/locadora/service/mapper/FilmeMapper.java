package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.FilmeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Filme and its DTO FilmeDTO.
 */
@Mapper(componentModel = "spring", uses = {DiretorMapper.class})
public interface FilmeMapper extends EntityMapper<FilmeDTO, Filme> {

    @Mapping(source = "diretor.id", target = "diretorId")
    @Mapping(source = "diretor.nome", target = "diretorNome")
    FilmeDTO toDto(Filme filme);

    @Mapping(source = "diretorId", target = "diretor")
    @Mapping(target = "midias", ignore = true)
    @Mapping(target = "ators", ignore = true)
    Filme toEntity(FilmeDTO filmeDTO);

    default Filme fromId(Long id) {
        if (id == null) {
            return null;
        }
        Filme filme = new Filme();
        filme.setId(id);
        return filme;
    }
}
