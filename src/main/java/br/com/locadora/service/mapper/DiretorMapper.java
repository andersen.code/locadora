package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.DiretorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Diretor and its DTO DiretorDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DiretorMapper extends EntityMapper<DiretorDTO, Diretor> {


    @Mapping(target = "filmes", ignore = true)
    Diretor toEntity(DiretorDTO diretorDTO);

    default Diretor fromId(Long id) {
        if (id == null) {
            return null;
        }
        Diretor diretor = new Diretor();
        diretor.setId(id);
        return diretor;
    }
}
