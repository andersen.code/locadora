package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.DadosPessoaisDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DadosPessoais and its DTO DadosPessoaisDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface DadosPessoaisMapper extends EntityMapper<DadosPessoaisDTO, DadosPessoais> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.email", target = "userEmail")
    DadosPessoaisDTO toDto(DadosPessoais dadosPessoais);

    @Mapping(source = "userId", target = "user")
    DadosPessoais toEntity(DadosPessoaisDTO dadosPessoaisDTO);

    default DadosPessoais fromId(Long id) {
        if (id == null) {
            return null;
        }
        DadosPessoais dadosPessoais = new DadosPessoais();
        dadosPessoais.setId(id);
        return dadosPessoais;
    }
}
