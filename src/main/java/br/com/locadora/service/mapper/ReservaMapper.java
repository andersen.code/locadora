package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.ReservaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Reserva and its DTO ReservaDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface ReservaMapper extends EntityMapper<ReservaDTO, Reserva> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.email", target = "userEmail")
    ReservaDTO toDto(Reserva reserva);

    @Mapping(source = "userId", target = "user")
    Reserva toEntity(ReservaDTO reservaDTO);

    default Reserva fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reserva reserva = new Reserva();
        reserva.setId(id);
        return reserva;
    }
}
