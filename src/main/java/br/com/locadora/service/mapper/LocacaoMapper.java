package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.LocacaoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Locacao and its DTO LocacaoDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface LocacaoMapper extends EntityMapper<LocacaoDTO, Locacao> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.email", target = "userEmail")
    LocacaoDTO toDto(Locacao locacao);

    @Mapping(target = "itensLocacaos", ignore = true)
    @Mapping(source = "userId", target = "user")
    Locacao toEntity(LocacaoDTO locacaoDTO);

    default Locacao fromId(Long id) {
        if (id == null) {
            return null;
        }
        Locacao locacao = new Locacao();
        locacao.setId(id);
        return locacao;
    }
}
