package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.AtorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Ator and its DTO AtorDTO.
 */
@Mapper(componentModel = "spring", uses = {FilmeMapper.class})
public interface AtorMapper extends EntityMapper<AtorDTO, Ator> {



    default Ator fromId(Long id) {
        if (id == null) {
            return null;
        }
        Ator ator = new Ator();
        ator.setId(id);
        return ator;
    }
}
