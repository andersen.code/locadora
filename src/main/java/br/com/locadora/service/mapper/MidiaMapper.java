package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.MidiaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Midia and its DTO MidiaDTO.
 */
@Mapper(componentModel = "spring", uses = {FilmeMapper.class})
public interface MidiaMapper extends EntityMapper<MidiaDTO, Midia> {


    @Mapping(target = "itensLocacaos", ignore = true)
    Midia toEntity(MidiaDTO midiaDTO);

    default Midia fromId(Long id) {
        if (id == null) {
            return null;
        }
        Midia midia = new Midia();
        midia.setId(id);
        return midia;
    }
}
