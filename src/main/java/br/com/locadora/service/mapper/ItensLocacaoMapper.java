package br.com.locadora.service.mapper;

import br.com.locadora.domain.*;
import br.com.locadora.service.dto.ItensLocacaoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ItensLocacao and its DTO ItensLocacaoDTO.
 */
@Mapper(componentModel = "spring", uses = {MidiaMapper.class, LocacaoMapper.class})
public interface ItensLocacaoMapper extends EntityMapper<ItensLocacaoDTO, ItensLocacao> {

    @Mapping(source = "midia.id", target = "midiaId")
    @Mapping(source = "midia.tipo", target = "midiaTipo")
    @Mapping(source = "locacao.id", target = "locacaoId")
    ItensLocacaoDTO toDto(ItensLocacao itensLocacao);

    @Mapping(source = "midiaId", target = "midia")
    @Mapping(source = "locacaoId", target = "locacao")
    ItensLocacao toEntity(ItensLocacaoDTO itensLocacaoDTO);

    default ItensLocacao fromId(Long id) {
        if (id == null) {
            return null;
        }
        ItensLocacao itensLocacao = new ItensLocacao();
        itensLocacao.setId(id);
        return itensLocacao;
    }
}
