package br.com.locadora.service;

import br.com.locadora.service.dto.FilmeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Filme.
 */
public interface FilmeService {

    /**
     * Save a filme.
     *
     * @param filmeDTO the entity to save
     * @return the persisted entity
     */
    FilmeDTO save(FilmeDTO filmeDTO);

    /**
     * Get all the filmes disponíveis.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<FilmeDTO> findAll(Pageable pageable);


    /**
     * Get all the filmes .
     *
     * @param pageable the pagination information
     * @return the list of entities
     */

    Page<FilmeDTO> findAllFilmesDisponivel(Pageable pageable);


    /**
     * Get the "id" filme.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<FilmeDTO> findOne(Long id);

    /**
     * Delete the "id" filme.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
