package br.com.locadora.service;

import br.com.locadora.service.dto.ItensLocacaoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing ItensLocacao.
 */
public interface ItensLocacaoService {

    /**
     * Save a itensLocacao.
     *
     * @param itensLocacaoDTO the entity to save
     * @return the persisted entity
     */
    ItensLocacaoDTO save(ItensLocacaoDTO itensLocacaoDTO);

    /**
     * Get all the itensLocacaos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ItensLocacaoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" itensLocacao.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ItensLocacaoDTO> findOne(Long id);

    /**
     * Delete the "id" itensLocacao.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
