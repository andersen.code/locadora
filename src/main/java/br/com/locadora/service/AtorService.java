package br.com.locadora.service;

import br.com.locadora.service.dto.AtorDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Ator.
 */
public interface AtorService {

    /**
     * Save a ator.
     *
     * @param atorDTO the entity to save
     * @return the persisted entity
     */
    AtorDTO save(AtorDTO atorDTO);

    /**
     * Get all the ators.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AtorDTO> findAll(Pageable pageable);

    /**
     * Get all the Ator with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    Page<AtorDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" ator.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AtorDTO> findOne(Long id);

    /**
     * Delete the "id" ator.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
