package br.com.locadora.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ItensLocacao entity.
 */
public class ItensLocacaoDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate dataEntregaPrevista;

    private LocalDate dataEntregaEfetiva;


    private Long midiaId;

    private String midiaTipo;

    private Long locacaoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataEntregaPrevista() {
        return dataEntregaPrevista;
    }

    public void setDataEntregaPrevista(LocalDate dataEntregaPrevista) {
        this.dataEntregaPrevista = dataEntregaPrevista;
    }

    public LocalDate getDataEntregaEfetiva() {
        return dataEntregaEfetiva;
    }

    public void setDataEntregaEfetiva(LocalDate dataEntregaEfetiva) {
        this.dataEntregaEfetiva = dataEntregaEfetiva;
    }

    public Long getMidiaId() {
        return midiaId;
    }

    public void setMidiaId(Long midiaId) {
        this.midiaId = midiaId;
    }

    public String getMidiaTipo() {
        return midiaTipo;
    }

    public void setMidiaTipo(String midiaTipo) {
        this.midiaTipo = midiaTipo;
    }

    public Long getLocacaoId() {
        return locacaoId;
    }

    public void setLocacaoId(Long locacaoId) {
        this.locacaoId = locacaoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ItensLocacaoDTO itensLocacaoDTO = (ItensLocacaoDTO) o;
        if (itensLocacaoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), itensLocacaoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ItensLocacaoDTO{" +
            "id=" + getId() +
            ", dataEntregaPrevista='" + getDataEntregaPrevista() + "'" +
            ", dataEntregaEfetiva='" + getDataEntregaEfetiva() + "'" +
            ", midia=" + getMidiaId() +
            ", midia='" + getMidiaTipo() + "'" +
            ", locacao=" + getLocacaoId() +
            "}";
    }
}
