package br.com.locadora.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import br.com.locadora.domain.enumeration.Categoria;

/**
 * A DTO for the Filme entity.
 */
public class FilmeDTO implements Serializable {

    private Long id;

    @NotNull
    private String titulo;

    private Categoria categoria;


    private Long diretorId;

    private String diretorNome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Long getDiretorId() {
        return diretorId;
    }

    public void setDiretorId(Long diretorId) {
        this.diretorId = diretorId;
    }

    public String getDiretorNome() {
        return diretorNome;
    }

    public void setDiretorNome(String diretorNome) {
        this.diretorNome = diretorNome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FilmeDTO filmeDTO = (FilmeDTO) o;
        if (filmeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), filmeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FilmeDTO{" +
            "id=" + getId() +
            ", titulo='" + getTitulo() + "'" +
            ", categoria='" + getCategoria() + "'" +
            ", diretor=" + getDiretorId() +
            ", diretor='" + getDiretorNome() + "'" +
            "}";
    }
}
