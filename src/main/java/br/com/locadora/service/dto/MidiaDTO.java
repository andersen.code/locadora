package br.com.locadora.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import br.com.locadora.domain.enumeration.IdiomaFilme;
import br.com.locadora.domain.enumeration.TipoMidia;
import br.com.locadora.domain.enumeration.StatusMidia;

/**
 * A DTO for the Midia entity.
 */
public class MidiaDTO implements Serializable {

    private Long id;

    @NotNull
    private IdiomaFilme idioma;

    @NotNull
    private TipoMidia tipo;

    @NotNull
    private StatusMidia statusMidia;


    private Set<FilmeDTO> filmes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdiomaFilme getIdioma() {
        return idioma;
    }

    public void setIdioma(IdiomaFilme idioma) {
        this.idioma = idioma;
    }

    public TipoMidia getTipo() {
        return tipo;
    }

    public void setTipo(TipoMidia tipo) {
        this.tipo = tipo;
    }

    public StatusMidia getStatusMidia() {
        return statusMidia;
    }

    public void setStatusMidia(StatusMidia statusMidia) {
        this.statusMidia = statusMidia;
    }

    public Set<FilmeDTO> getFilmes() {
        return filmes;
    }

    public void setFilmes(Set<FilmeDTO> filmes) {
        this.filmes = filmes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MidiaDTO midiaDTO = (MidiaDTO) o;
        if (midiaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), midiaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MidiaDTO{" +
            "id=" + getId() +
            ", idioma='" + getIdioma() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", statusMidia='" + getStatusMidia() + "'" +
            "}";
    }
}
