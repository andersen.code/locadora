package br.com.locadora.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Diretor entity.
 */
public class DiretorDTO implements Serializable {

    private Long id;

    @NotNull
    private String nome;

    @Size(max = 4000)
    private String observacao;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DiretorDTO diretorDTO = (DiretorDTO) o;
        if (diretorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), diretorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DiretorDTO{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", observacao='" + getObservacao() + "'" +
            "}";
    }
}
