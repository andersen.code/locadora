package br.com.locadora.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import br.com.locadora.domain.enumeration.TipoPessoa;
import br.com.locadora.domain.enumeration.ClienteSituacao;

/**
 * A DTO for the DadosPessoais entity.
 */
public class DadosPessoaisDTO implements Serializable {

    private Long id;

    @NotNull
    private TipoPessoa tipoPessoa;

    @NotNull
    @Size(min = 5, max = 120)
    private String nome;

    @NotNull
    @Size(min = 11, max = 14)
    private String documentoReceitaFederal;

    private ClienteSituacao situacao;

    @Size(max = 4000)
    private String observacao;


    private Long userId;

    private String userEmail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDocumentoReceitaFederal() {
        return documentoReceitaFederal;
    }

    public void setDocumentoReceitaFederal(String documentoReceitaFederal) {
        this.documentoReceitaFederal = documentoReceitaFederal;
    }

    public ClienteSituacao getSituacao() {
        return situacao;
    }

    public void setSituacao(ClienteSituacao situacao) {
        this.situacao = situacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DadosPessoaisDTO dadosPessoaisDTO = (DadosPessoaisDTO) o;
        if (dadosPessoaisDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dadosPessoaisDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DadosPessoaisDTO{" +
            "id=" + getId() +
            ", tipoPessoa='" + getTipoPessoa() + "'" +
            ", nome='" + getNome() + "'" +
            ", documentoReceitaFederal='" + getDocumentoReceitaFederal() + "'" +
            ", situacao='" + getSituacao() + "'" +
            ", observacao='" + getObservacao() + "'" +
            ", user=" + getUserId() +
            ", user='" + getUserEmail() + "'" +
            "}";
    }
}
