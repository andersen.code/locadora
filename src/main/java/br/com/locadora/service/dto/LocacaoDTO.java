package br.com.locadora.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the Locacao entity.
 */
public class LocacaoDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate dataLocacao;

    private BigDecimal valorLocacao;

    @Size(max = 4000)
    private String observacao;


    private Long userId;

    private String userEmail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(LocalDate dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public BigDecimal getValorLocacao() {
        return valorLocacao;
    }

    public void setValorLocacao(BigDecimal valorLocacao) {
        this.valorLocacao = valorLocacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LocacaoDTO locacaoDTO = (LocacaoDTO) o;
        if (locacaoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), locacaoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LocacaoDTO{" +
            "id=" + getId() +
            ", dataLocacao='" + getDataLocacao() + "'" +
            ", valorLocacao=" + getValorLocacao() +
            ", observacao='" + getObservacao() + "'" +
            ", user=" + getUserId() +
            ", user='" + getUserEmail() + "'" +
            "}";
    }
}
