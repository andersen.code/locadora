package br.com.locadora.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Ator entity.
 */
public class AtorDTO implements Serializable {

    private Long id;

    @NotNull
    private String nome;


    private Set<FilmeDTO> filmes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<FilmeDTO> getFilmes() {
        return filmes;
    }

    public void setFilmes(Set<FilmeDTO> filmes) {
        this.filmes = filmes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AtorDTO atorDTO = (AtorDTO) o;
        if (atorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), atorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AtorDTO{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            "}";
    }
}
