package br.com.locadora.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Reserva entity.
 */
public class ReservaDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate dataLocacaoPrevista;

    @NotNull
    private LocalDate dataValidadeReserva;

    @Size(max = 4000)
    private String observacao;


    private Long userId;

    private String userEmail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataLocacaoPrevista() {
        return dataLocacaoPrevista;
    }

    public void setDataLocacaoPrevista(LocalDate dataLocacaoPrevista) {
        this.dataLocacaoPrevista = dataLocacaoPrevista;
    }

    public LocalDate getDataValidadeReserva() {
        return dataValidadeReserva;
    }

    public void setDataValidadeReserva(LocalDate dataValidadeReserva) {
        this.dataValidadeReserva = dataValidadeReserva;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReservaDTO reservaDTO = (ReservaDTO) o;
        if (reservaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReservaDTO{" +
            "id=" + getId() +
            ", dataLocacaoPrevista='" + getDataLocacaoPrevista() + "'" +
            ", dataValidadeReserva='" + getDataValidadeReserva() + "'" +
            ", observacao='" + getObservacao() + "'" +
            ", user=" + getUserId() +
            ", user='" + getUserEmail() + "'" +
            "}";
    }
}
