package br.com.locadora.repository;

import br.com.locadora.domain.Midia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Midia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MidiaRepository extends JpaRepository<Midia, Long> {

    @Query(value = "select distinct midia from Midia midia left join fetch midia.filmes",
        countQuery = "select count(distinct midia) from Midia midia")
    Page<Midia> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct midia from Midia midia left join fetch midia.filmes")
    List<Midia> findAllWithEagerRelationships();

    @Query("select midia from Midia midia left join fetch midia.filmes where midia.id =:id")
    Optional<Midia> findOneWithEagerRelationships(@Param("id") Long id);

}
