package br.com.locadora.repository;

import br.com.locadora.domain.Locacao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Locacao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocacaoRepository extends JpaRepository<Locacao, Long> {

    @Query("select locacao from Locacao locacao where locacao.user.login = ?#{principal.username}")
    List<Locacao> findByUserIsCurrentUser();

}
