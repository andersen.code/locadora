package br.com.locadora.repository;

import br.com.locadora.domain.Ator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Ator entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtorRepository extends JpaRepository<Ator, Long> {

    @Query(value = "select distinct ator from Ator ator left join fetch ator.filmes",
        countQuery = "select count(distinct ator) from Ator ator")
    Page<Ator> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct ator from Ator ator left join fetch ator.filmes")
    List<Ator> findAllWithEagerRelationships();

    @Query("select ator from Ator ator left join fetch ator.filmes where ator.id =:id")
    Optional<Ator> findOneWithEagerRelationships(@Param("id") Long id);

}
