package br.com.locadora.repository;

import br.com.locadora.domain.ItensLocacao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ItensLocacao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ItensLocacaoRepository extends JpaRepository<ItensLocacao, Long> {

}
