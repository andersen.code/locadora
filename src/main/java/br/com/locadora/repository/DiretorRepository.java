package br.com.locadora.repository;

import br.com.locadora.domain.Diretor;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Diretor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiretorRepository extends JpaRepository<Diretor, Long> {

}
