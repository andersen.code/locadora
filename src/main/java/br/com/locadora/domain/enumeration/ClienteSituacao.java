package br.com.locadora.domain.enumeration;

/**
 * The ClienteSituacao enumeration.
 */
public enum ClienteSituacao {
    ATIVO, INATIVO, BLOQUEADO
}
