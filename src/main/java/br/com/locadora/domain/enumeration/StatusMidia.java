package br.com.locadora.domain.enumeration;

/**
 * The StatusMidia enumeration.
 */
public enum StatusMidia {
    LOCADA, RESERVADA, DISPONIVEL, OUTROS
}
