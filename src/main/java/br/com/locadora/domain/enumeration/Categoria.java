package br.com.locadora.domain.enumeration;

/**
 * The Categoria enumeration.
 */
public enum Categoria {
    COMEDIA, ACAO, AVENTURA, ANIME, FAMILIA, KIDS, CULT, DOCUMENTARIOS, DRAMAS, TERROS
}
