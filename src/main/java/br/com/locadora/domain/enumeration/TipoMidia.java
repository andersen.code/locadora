package br.com.locadora.domain.enumeration;

/**
 * The TipoMidia enumeration.
 */
public enum TipoMidia {
    DVD, CD, VHS, BLU_RAY, DISCO_VINIL, K7
}
