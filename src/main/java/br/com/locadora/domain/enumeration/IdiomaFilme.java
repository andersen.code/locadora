package br.com.locadora.domain.enumeration;

/**
 * The IdiomaFilme enumeration.
 */
public enum IdiomaFilme {
    PORTUGUES, ESPANHOL, INGLES
}
