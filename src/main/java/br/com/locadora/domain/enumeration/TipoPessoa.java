package br.com.locadora.domain.enumeration;

/**
 * The TipoPessoa enumeration.
 */
public enum TipoPessoa {
    FISICA, JURIDICA
}
