package br.com.locadora.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import br.com.locadora.domain.enumeration.TipoPessoa;

import br.com.locadora.domain.enumeration.ClienteSituacao;

/**
 * A DadosPessoais.
 */
@Entity
@Table(name = "dados_pessoais")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DadosPessoais implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_pessoa", nullable = false)
    private TipoPessoa tipoPessoa;

    @NotNull
    @Size(min = 5, max = 120)
    @Column(name = "nome", length = 120, nullable = false)
    private String nome;

    @NotNull
    @Size(min = 11, max = 14)
    @Column(name = "documento_receita_federal", length = 14, nullable = false)
    private String documentoReceitaFederal;

    @Enumerated(EnumType.STRING)
    @Column(name = "situacao")
    private ClienteSituacao situacao;

    @Size(max = 4000)
    @Column(name = "observacao", length = 4000)
    private String observacao;

    @OneToOne(optional = false)    @NotNull

    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    public DadosPessoais tipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
        return this;
    }

    public void setTipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getNome() {
        return nome;
    }

    public DadosPessoais nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDocumentoReceitaFederal() {
        return documentoReceitaFederal;
    }

    public DadosPessoais documentoReceitaFederal(String documentoReceitaFederal) {
        this.documentoReceitaFederal = documentoReceitaFederal;
        return this;
    }

    public void setDocumentoReceitaFederal(String documentoReceitaFederal) {
        this.documentoReceitaFederal = documentoReceitaFederal;
    }

    public ClienteSituacao getSituacao() {
        return situacao;
    }

    public DadosPessoais situacao(ClienteSituacao situacao) {
        this.situacao = situacao;
        return this;
    }

    public void setSituacao(ClienteSituacao situacao) {
        this.situacao = situacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public DadosPessoais observacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public User getUser() {
        return user;
    }

    public DadosPessoais user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DadosPessoais dadosPessoais = (DadosPessoais) o;
        if (dadosPessoais.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dadosPessoais.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DadosPessoais{" +
            "id=" + getId() +
            ", tipoPessoa='" + getTipoPessoa() + "'" +
            ", nome='" + getNome() + "'" +
            ", documentoReceitaFederal='" + getDocumentoReceitaFederal() + "'" +
            ", situacao='" + getSituacao() + "'" +
            ", observacao='" + getObservacao() + "'" +
            "}";
    }
}
