package br.com.locadora.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ItensLocacao.
 */
@Entity
@Table(name = "itens_locacao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ItensLocacao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "data_entrega_prevista", nullable = false)
    private LocalDate dataEntregaPrevista;

    @Column(name = "data_entrega_efetiva")
    private LocalDate dataEntregaEfetiva;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("itensLocacaos")
    private Midia midia;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("itensLocacaos")
    private Locacao locacao;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataEntregaPrevista() {
        return dataEntregaPrevista;
    }

    public ItensLocacao dataEntregaPrevista(LocalDate dataEntregaPrevista) {
        this.dataEntregaPrevista = dataEntregaPrevista;
        return this;
    }

    public void setDataEntregaPrevista(LocalDate dataEntregaPrevista) {
        this.dataEntregaPrevista = dataEntregaPrevista;
    }

    public LocalDate getDataEntregaEfetiva() {
        return dataEntregaEfetiva;
    }

    public ItensLocacao dataEntregaEfetiva(LocalDate dataEntregaEfetiva) {
        this.dataEntregaEfetiva = dataEntregaEfetiva;
        return this;
    }

    public void setDataEntregaEfetiva(LocalDate dataEntregaEfetiva) {
        this.dataEntregaEfetiva = dataEntregaEfetiva;
    }

    public Midia getMidia() {
        return midia;
    }

    public ItensLocacao midia(Midia midia) {
        this.midia = midia;
        return this;
    }

    public void setMidia(Midia midia) {
        this.midia = midia;
    }

    public Locacao getLocacao() {
        return locacao;
    }

    public ItensLocacao locacao(Locacao locacao) {
        this.locacao = locacao;
        return this;
    }

    public void setLocacao(Locacao locacao) {
        this.locacao = locacao;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ItensLocacao itensLocacao = (ItensLocacao) o;
        if (itensLocacao.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), itensLocacao.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ItensLocacao{" +
            "id=" + getId() +
            ", dataEntregaPrevista='" + getDataEntregaPrevista() + "'" +
            ", dataEntregaEfetiva='" + getDataEntregaEfetiva() + "'" +
            "}";
    }
}
