package br.com.locadora.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Ator.
 */
@Entity
@Table(name = "ator")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Ator implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "ator_filme",
               joinColumns = @JoinColumn(name = "ator_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "filme_id", referencedColumnName = "id"))
    private Set<Filme> filmes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Ator nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<Filme> getFilmes() {
        return filmes;
    }

    public Ator filmes(Set<Filme> filmes) {
        this.filmes = filmes;
        return this;
    }

    public Ator addFilme(Filme filme) {
        this.filmes.add(filme);
        filme.getAtors().add(this);
        return this;
    }

    public Ator removeFilme(Filme filme) {
        this.filmes.remove(filme);
        filme.getAtors().remove(this);
        return this;
    }

    public void setFilmes(Set<Filme> filmes) {
        this.filmes = filmes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ator ator = (Ator) o;
        if (ator.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ator.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Ator{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            "}";
    }
}
