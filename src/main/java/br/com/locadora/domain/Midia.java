package br.com.locadora.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import br.com.locadora.domain.enumeration.IdiomaFilme;

import br.com.locadora.domain.enumeration.TipoMidia;

import br.com.locadora.domain.enumeration.StatusMidia;

/**
 * A Midia.
 */
@Entity
@Table(name = "midia")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Midia implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "idioma", nullable = false)
    private IdiomaFilme idioma;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo", nullable = false)
    private TipoMidia tipo;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status_midia", nullable = false)
    private StatusMidia statusMidia;

    @OneToMany(mappedBy = "midia")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ItensLocacao> itensLocacaos = new HashSet<>();
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "midia_filme",
               joinColumns = @JoinColumn(name = "midia_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "filme_id", referencedColumnName = "id"))
    private Set<Filme> filmes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdiomaFilme getIdioma() {
        return idioma;
    }

    public Midia idioma(IdiomaFilme idioma) {
        this.idioma = idioma;
        return this;
    }

    public void setIdioma(IdiomaFilme idioma) {
        this.idioma = idioma;
    }

    public TipoMidia getTipo() {
        return tipo;
    }

    public Midia tipo(TipoMidia tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(TipoMidia tipo) {
        this.tipo = tipo;
    }

    public StatusMidia getStatusMidia() {
        return statusMidia;
    }

    public Midia statusMidia(StatusMidia statusMidia) {
        this.statusMidia = statusMidia;
        return this;
    }

    public void setStatusMidia(StatusMidia statusMidia) {
        this.statusMidia = statusMidia;
    }

    public Set<ItensLocacao> getItensLocacaos() {
        return itensLocacaos;
    }

    public Midia itensLocacaos(Set<ItensLocacao> itensLocacaos) {
        this.itensLocacaos = itensLocacaos;
        return this;
    }

    public Midia addItensLocacao(ItensLocacao itensLocacao) {
        this.itensLocacaos.add(itensLocacao);
        itensLocacao.setMidia(this);
        return this;
    }

    public Midia removeItensLocacao(ItensLocacao itensLocacao) {
        this.itensLocacaos.remove(itensLocacao);
        itensLocacao.setMidia(null);
        return this;
    }

    public void setItensLocacaos(Set<ItensLocacao> itensLocacaos) {
        this.itensLocacaos = itensLocacaos;
    }

    public Set<Filme> getFilmes() {
        return filmes;
    }

    public Midia filmes(Set<Filme> filmes) {
        this.filmes = filmes;
        return this;
    }

    public Midia addFilme(Filme filme) {
        this.filmes.add(filme);
        filme.getMidias().add(this);
        return this;
    }

    public Midia removeFilme(Filme filme) {
        this.filmes.remove(filme);
        filme.getMidias().remove(this);
        return this;
    }

    public void setFilmes(Set<Filme> filmes) {
        this.filmes = filmes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Midia midia = (Midia) o;
        if (midia.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), midia.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Midia{" +
            "id=" + getId() +
            ", idioma='" + getIdioma() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", statusMidia='" + getStatusMidia() + "'" +
            "}";
    }
}
