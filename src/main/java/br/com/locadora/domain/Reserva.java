package br.com.locadora.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Reserva.
 */
@Entity
@Table(name = "reserva")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "data_locacao_prevista", nullable = false)
    private LocalDate dataLocacaoPrevista;

    @NotNull
    @Column(name = "data_validade_reserva", nullable = false)
    private LocalDate dataValidadeReserva;

    @Size(max = 4000)
    @Column(name = "observacao", length = 4000)
    private String observacao;

    @ManyToOne
    @JsonIgnoreProperties("reservas")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataLocacaoPrevista() {
        return dataLocacaoPrevista;
    }

    public Reserva dataLocacaoPrevista(LocalDate dataLocacaoPrevista) {
        this.dataLocacaoPrevista = dataLocacaoPrevista;
        return this;
    }

    public void setDataLocacaoPrevista(LocalDate dataLocacaoPrevista) {
        this.dataLocacaoPrevista = dataLocacaoPrevista;
    }

    public LocalDate getDataValidadeReserva() {
        return dataValidadeReserva;
    }

    public Reserva dataValidadeReserva(LocalDate dataValidadeReserva) {
        this.dataValidadeReserva = dataValidadeReserva;
        return this;
    }

    public void setDataValidadeReserva(LocalDate dataValidadeReserva) {
        this.dataValidadeReserva = dataValidadeReserva;
    }

    public String getObservacao() {
        return observacao;
    }

    public Reserva observacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public User getUser() {
        return user;
    }

    public Reserva user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reserva reserva = (Reserva) o;
        if (reserva.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reserva.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reserva{" +
            "id=" + getId() +
            ", dataLocacaoPrevista='" + getDataLocacaoPrevista() + "'" +
            ", dataValidadeReserva='" + getDataValidadeReserva() + "'" +
            ", observacao='" + getObservacao() + "'" +
            "}";
    }
}
