package br.com.locadora.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import br.com.locadora.domain.enumeration.Categoria;

/**
 * A Filme.
 */
@Entity
@Table(name = "filme")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Filme implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "titulo", nullable = false)
    private String titulo;

    @Enumerated(EnumType.STRING)
    @Column(name = "categoria")
    private Categoria categoria;

    @ManyToOne
    @JsonIgnoreProperties("filmes")
    private Diretor diretor;

    @ManyToMany(mappedBy = "filmes")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Midia> midias = new HashSet<>();

    @ManyToMany(mappedBy = "filmes")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Ator> ators = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public Filme titulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Filme categoria(Categoria categoria) {
        this.categoria = categoria;
        return this;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Diretor getDiretor() {
        return diretor;
    }

    public Filme diretor(Diretor diretor) {
        this.diretor = diretor;
        return this;
    }

    public void setDiretor(Diretor diretor) {
        this.diretor = diretor;
    }

    public Set<Midia> getMidias() {
        return midias;
    }

    public Filme midias(Set<Midia> midias) {
        this.midias = midias;
        return this;
    }

    public Filme addMidia(Midia midia) {
        this.midias.add(midia);
        midia.getFilmes().add(this);
        return this;
    }

    public Filme removeMidia(Midia midia) {
        this.midias.remove(midia);
        midia.getFilmes().remove(this);
        return this;
    }

    public void setMidias(Set<Midia> midias) {
        this.midias = midias;
    }

    public Set<Ator> getAtors() {
        return ators;
    }

    public Filme ators(Set<Ator> ators) {
        this.ators = ators;
        return this;
    }

    public Filme addAtor(Ator ator) {
        this.ators.add(ator);
        ator.getFilmes().add(this);
        return this;
    }

    public Filme removeAtor(Ator ator) {
        this.ators.remove(ator);
        ator.getFilmes().remove(this);
        return this;
    }

    public void setAtors(Set<Ator> ators) {
        this.ators = ators;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Filme filme = (Filme) o;
        if (filme.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), filme.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Filme{" +
            "id=" + getId() +
            ", titulo='" + getTitulo() + "'" +
            ", categoria='" + getCategoria() + "'" +
            "}";
    }
}
