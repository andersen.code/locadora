package br.com.locadora.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Diretor.
 */
@Entity
@Table(name = "diretor")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Diretor implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @Size(max = 4000)
    @Column(name = "observacao", length = 4000)
    private String observacao;

    @OneToMany(mappedBy = "diretor")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Filme> filmes = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Diretor nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public Diretor observacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Set<Filme> getFilmes() {
        return filmes;
    }

    public Diretor filmes(Set<Filme> filmes) {
        this.filmes = filmes;
        return this;
    }

    public Diretor addFilme(Filme filme) {
        this.filmes.add(filme);
        filme.setDiretor(this);
        return this;
    }

    public Diretor removeFilme(Filme filme) {
        this.filmes.remove(filme);
        filme.setDiretor(null);
        return this;
    }

    public void setFilmes(Set<Filme> filmes) {
        this.filmes = filmes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Diretor diretor = (Diretor) o;
        if (diretor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), diretor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Diretor{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", observacao='" + getObservacao() + "'" +
            "}";
    }
}
