package br.com.locadora.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Locacao.
 */
@Entity
@Table(name = "locacao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Locacao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "data_locacao", nullable = false)
    private LocalDate dataLocacao;

    @Column(name = "valor_locacao", precision = 10, scale = 2)
    private BigDecimal valorLocacao;

    @Size(max = 4000)
    @Column(name = "observacao", length = 4000)
    private String observacao;

    @OneToMany(mappedBy = "locacao")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ItensLocacao> itensLocacaos = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("locacaos")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataLocacao() {
        return dataLocacao;
    }

    public Locacao dataLocacao(LocalDate dataLocacao) {
        this.dataLocacao = dataLocacao;
        return this;
    }

    public void setDataLocacao(LocalDate dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public BigDecimal getValorLocacao() {
        return valorLocacao;
    }

    public Locacao valorLocacao(BigDecimal valorLocacao) {
        this.valorLocacao = valorLocacao;
        return this;
    }

    public void setValorLocacao(BigDecimal valorLocacao) {
        this.valorLocacao = valorLocacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public Locacao observacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Set<ItensLocacao> getItensLocacaos() {
        return itensLocacaos;
    }

    public Locacao itensLocacaos(Set<ItensLocacao> itensLocacaos) {
        this.itensLocacaos = itensLocacaos;
        return this;
    }

    public Locacao addItensLocacao(ItensLocacao itensLocacao) {
        this.itensLocacaos.add(itensLocacao);
        itensLocacao.setLocacao(this);
        return this;
    }

    public Locacao removeItensLocacao(ItensLocacao itensLocacao) {
        this.itensLocacaos.remove(itensLocacao);
        itensLocacao.setLocacao(null);
        return this;
    }

    public void setItensLocacaos(Set<ItensLocacao> itensLocacaos) {
        this.itensLocacaos = itensLocacaos;
    }

    public User getUser() {
        return user;
    }

    public Locacao user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Locacao locacao = (Locacao) o;
        if (locacao.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), locacao.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Locacao{" +
            "id=" + getId() +
            ", dataLocacao='" + getDataLocacao() + "'" +
            ", valorLocacao=" + getValorLocacao() +
            ", observacao='" + getObservacao() + "'" +
            "}";
    }
}
