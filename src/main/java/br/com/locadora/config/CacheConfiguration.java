package br.com.locadora.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(br.com.locadora.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(br.com.locadora.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.DadosPessoais.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Filme.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Filme.class.getName() + ".midias", jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Midia.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Midia.class.getName() + ".itensLocacaos", jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Locacao.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.ItensLocacao.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Reserva.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Diretor.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Diretor.class.getName() + ".filmes", jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Ator.class.getName(), jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Ator.class.getName() + ".filmes", jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Filme.class.getName() + ".ators", jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Midia.class.getName() + ".filmes", jcacheConfiguration);
            cm.createCache(br.com.locadora.domain.Locacao.class.getName() + ".itensLocacaos", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
