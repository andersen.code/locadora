package br.com.locadora.web.rest;
import br.com.locadora.service.FilmeService;
import br.com.locadora.web.rest.errors.BadRequestAlertException;
import br.com.locadora.web.rest.util.HeaderUtil;
import br.com.locadora.web.rest.util.PaginationUtil;
import br.com.locadora.service.dto.FilmeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Filme.
 */
@RestController
@RequestMapping("/api")
public class FilmeResource {

    private final Logger log = LoggerFactory.getLogger(FilmeResource.class);

    private static final String ENTITY_NAME = "filme";

    private final FilmeService filmeService;

    public FilmeResource(FilmeService filmeService) {
        this.filmeService = filmeService;
    }

    /**
     * POST  /filmes : Create a new filme.
     *
     * @param filmeDTO the filmeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new filmeDTO, or with status 400 (Bad Request) if the filme has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/filmes")
    public ResponseEntity<FilmeDTO> createFilme(@Valid @RequestBody FilmeDTO filmeDTO) throws URISyntaxException {
        log.debug("REST request to save Filme : {}", filmeDTO);
        if (filmeDTO.getId() != null) {
            throw new BadRequestAlertException("A new filme cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FilmeDTO result = filmeService.save(filmeDTO);
        return ResponseEntity.created(new URI("/api/filmes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /filmes : Updates an existing filme.
     *
     * @param filmeDTO the filmeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated filmeDTO,
     * or with status 400 (Bad Request) if the filmeDTO is not valid,
     * or with status 500 (Internal Server Error) if the filmeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/filmes")
    public ResponseEntity<FilmeDTO> updateFilme(@Valid @RequestBody FilmeDTO filmeDTO) throws URISyntaxException {
        log.debug("REST request to update Filme : {}", filmeDTO);
        if (filmeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FilmeDTO result = filmeService.save(filmeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, filmeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /filmes : get all the filmes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of filmes in body
     */
    @GetMapping("/filmes")
    public ResponseEntity<List<FilmeDTO>> getAllFilmes(Pageable pageable) {
        log.debug("REST request to get a page of Filmes");
        Page<FilmeDTO> page = filmeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/filmes");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /filmes : get all the filmes disponíveis.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of filmes in body
     */
    @GetMapping("/filmesDisponivel")
    public ResponseEntity<List<FilmeDTO>> getAllFilmesDisponivel(Pageable pageable) {
        log.debug("REST request to get a page of Filmes");
        Page<FilmeDTO> page = filmeService.findAllFilmesDisponivel(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/filmes");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /filmes/:id : get the "id" filme.
     *
     * @param id the id of the filmeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the filmeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/filmes/{id}")
    public ResponseEntity<FilmeDTO> getFilme(@PathVariable Long id) {
        log.debug("REST request to get Filme : {}", id);
        Optional<FilmeDTO> filmeDTO = filmeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(filmeDTO);
    }

    /**
     * DELETE  /filmes/:id : delete the "id" filme.
     *
     * @param id the id of the filmeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/filmes/{id}")
    public ResponseEntity<Void> deleteFilme(@PathVariable Long id) {
        log.debug("REST request to delete Filme : {}", id);
        filmeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
