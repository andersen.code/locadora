package br.com.locadora.web.rest;
import br.com.locadora.service.ItensLocacaoService;
import br.com.locadora.web.rest.errors.BadRequestAlertException;
import br.com.locadora.web.rest.util.HeaderUtil;
import br.com.locadora.web.rest.util.PaginationUtil;
import br.com.locadora.service.dto.ItensLocacaoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ItensLocacao.
 */
@RestController
@RequestMapping("/api")
public class ItensLocacaoResource {

    private final Logger log = LoggerFactory.getLogger(ItensLocacaoResource.class);

    private static final String ENTITY_NAME = "itensLocacao";

    private final ItensLocacaoService itensLocacaoService;

    public ItensLocacaoResource(ItensLocacaoService itensLocacaoService) {
        this.itensLocacaoService = itensLocacaoService;
    }

    /**
     * POST  /itens-locacaos : Create a new itensLocacao.
     *
     * @param itensLocacaoDTO the itensLocacaoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new itensLocacaoDTO, or with status 400 (Bad Request) if the itensLocacao has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/itens-locacaos")
    public ResponseEntity<ItensLocacaoDTO> createItensLocacao(@Valid @RequestBody ItensLocacaoDTO itensLocacaoDTO) throws URISyntaxException {
        log.debug("REST request to save ItensLocacao : {}", itensLocacaoDTO);
        if (itensLocacaoDTO.getId() != null) {
            throw new BadRequestAlertException("A new itensLocacao cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ItensLocacaoDTO result = itensLocacaoService.save(itensLocacaoDTO);
        return ResponseEntity.created(new URI("/api/itens-locacaos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /itens-locacaos : Updates an existing itensLocacao.
     *
     * @param itensLocacaoDTO the itensLocacaoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated itensLocacaoDTO,
     * or with status 400 (Bad Request) if the itensLocacaoDTO is not valid,
     * or with status 500 (Internal Server Error) if the itensLocacaoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/itens-locacaos")
    public ResponseEntity<ItensLocacaoDTO> updateItensLocacao(@Valid @RequestBody ItensLocacaoDTO itensLocacaoDTO) throws URISyntaxException {
        log.debug("REST request to update ItensLocacao : {}", itensLocacaoDTO);
        if (itensLocacaoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ItensLocacaoDTO result = itensLocacaoService.save(itensLocacaoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, itensLocacaoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /itens-locacaos : get all the itensLocacaos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of itensLocacaos in body
     */
    @GetMapping("/itens-locacaos")
    public ResponseEntity<List<ItensLocacaoDTO>> getAllItensLocacaos(Pageable pageable) {
        log.debug("REST request to get a page of ItensLocacaos");
        Page<ItensLocacaoDTO> page = itensLocacaoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/itens-locacaos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /itens-locacaos/:id : get the "id" itensLocacao.
     *
     * @param id the id of the itensLocacaoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the itensLocacaoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/itens-locacaos/{id}")
    public ResponseEntity<ItensLocacaoDTO> getItensLocacao(@PathVariable Long id) {
        log.debug("REST request to get ItensLocacao : {}", id);
        Optional<ItensLocacaoDTO> itensLocacaoDTO = itensLocacaoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(itensLocacaoDTO);
    }

    /**
     * DELETE  /itens-locacaos/:id : delete the "id" itensLocacao.
     *
     * @param id the id of the itensLocacaoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/itens-locacaos/{id}")
    public ResponseEntity<Void> deleteItensLocacao(@PathVariable Long id) {
        log.debug("REST request to delete ItensLocacao : {}", id);
        itensLocacaoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
