package br.com.locadora.web.rest;
import br.com.locadora.service.MidiaService;
import br.com.locadora.web.rest.errors.BadRequestAlertException;
import br.com.locadora.web.rest.util.HeaderUtil;
import br.com.locadora.web.rest.util.PaginationUtil;
import br.com.locadora.service.dto.MidiaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Midia.
 */
@RestController
@RequestMapping("/api")
public class MidiaResource {

    private final Logger log = LoggerFactory.getLogger(MidiaResource.class);

    private static final String ENTITY_NAME = "midia";

    private final MidiaService midiaService;

    public MidiaResource(MidiaService midiaService) {
        this.midiaService = midiaService;
    }

    /**
     * POST  /midias : Create a new midia.
     *
     * @param midiaDTO the midiaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new midiaDTO, or with status 400 (Bad Request) if the midia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/midias")
    public ResponseEntity<MidiaDTO> createMidia(@Valid @RequestBody MidiaDTO midiaDTO) throws URISyntaxException {
        log.debug("REST request to save Midia : {}", midiaDTO);
        if (midiaDTO.getId() != null) {
            throw new BadRequestAlertException("A new midia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MidiaDTO result = midiaService.save(midiaDTO);
        return ResponseEntity.created(new URI("/api/midias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /midias : Updates an existing midia.
     *
     * @param midiaDTO the midiaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated midiaDTO,
     * or with status 400 (Bad Request) if the midiaDTO is not valid,
     * or with status 500 (Internal Server Error) if the midiaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/midias")
    public ResponseEntity<MidiaDTO> updateMidia(@Valid @RequestBody MidiaDTO midiaDTO) throws URISyntaxException {
        log.debug("REST request to update Midia : {}", midiaDTO);
        if (midiaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MidiaDTO result = midiaService.save(midiaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, midiaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /midias : get all the midias.
     *
     * @param pageable the pagination information
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of midias in body
     */
    @GetMapping("/midias")
    public ResponseEntity<List<MidiaDTO>> getAllMidias(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Midias");
        Page<MidiaDTO> page;
        if (eagerload) {
            page = midiaService.findAllWithEagerRelationships(pageable);
        } else {
            page = midiaService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/midias?eagerload=%b", eagerload));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /midias/:id : get the "id" midia.
     *
     * @param id the id of the midiaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the midiaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/midias/{id}")
    public ResponseEntity<MidiaDTO> getMidia(@PathVariable Long id) {
        log.debug("REST request to get Midia : {}", id);
        Optional<MidiaDTO> midiaDTO = midiaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(midiaDTO);
    }

    /**
     * DELETE  /midias/:id : delete the "id" midia.
     *
     * @param id the id of the midiaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/midias/{id}")
    public ResponseEntity<Void> deleteMidia(@PathVariable Long id) {
        log.debug("REST request to delete Midia : {}", id);
        midiaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
