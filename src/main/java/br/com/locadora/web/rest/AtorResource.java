package br.com.locadora.web.rest;
import br.com.locadora.service.AtorService;
import br.com.locadora.web.rest.errors.BadRequestAlertException;
import br.com.locadora.web.rest.util.HeaderUtil;
import br.com.locadora.web.rest.util.PaginationUtil;
import br.com.locadora.service.dto.AtorDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Ator.
 */
@RestController
@RequestMapping("/api")
public class AtorResource {

    private final Logger log = LoggerFactory.getLogger(AtorResource.class);

    private static final String ENTITY_NAME = "ator";

    private final AtorService atorService;

    public AtorResource(AtorService atorService) {
        this.atorService = atorService;
    }

    /**
     * POST  /ators : Create a new ator.
     *
     * @param atorDTO the atorDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new atorDTO, or with status 400 (Bad Request) if the ator has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ators")
    public ResponseEntity<AtorDTO> createAtor(@Valid @RequestBody AtorDTO atorDTO) throws URISyntaxException {
        log.debug("REST request to save Ator : {}", atorDTO);
        if (atorDTO.getId() != null) {
            throw new BadRequestAlertException("A new ator cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AtorDTO result = atorService.save(atorDTO);
        return ResponseEntity.created(new URI("/api/ators/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ators : Updates an existing ator.
     *
     * @param atorDTO the atorDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated atorDTO,
     * or with status 400 (Bad Request) if the atorDTO is not valid,
     * or with status 500 (Internal Server Error) if the atorDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ators")
    public ResponseEntity<AtorDTO> updateAtor(@Valid @RequestBody AtorDTO atorDTO) throws URISyntaxException {
        log.debug("REST request to update Ator : {}", atorDTO);
        if (atorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AtorDTO result = atorService.save(atorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, atorDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ators : get all the ators.
     *
     * @param pageable the pagination information
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of ators in body
     */
    @GetMapping("/ators")
    public ResponseEntity<List<AtorDTO>> getAllAtors(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Ators");
        Page<AtorDTO> page;
        if (eagerload) {
            page = atorService.findAllWithEagerRelationships(pageable);
        } else {
            page = atorService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/ators?eagerload=%b", eagerload));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /ators/:id : get the "id" ator.
     *
     * @param id the id of the atorDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the atorDTO, or with status 404 (Not Found)
     */
    @GetMapping("/ators/{id}")
    public ResponseEntity<AtorDTO> getAtor(@PathVariable Long id) {
        log.debug("REST request to get Ator : {}", id);
        Optional<AtorDTO> atorDTO = atorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(atorDTO);
    }

    /**
     * DELETE  /ators/:id : delete the "id" ator.
     *
     * @param id the id of the atorDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ators/{id}")
    public ResponseEntity<Void> deleteAtor(@PathVariable Long id) {
        log.debug("REST request to delete Ator : {}", id);
        atorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
