package br.com.locadora.web.rest;
import br.com.locadora.service.DiretorService;
import br.com.locadora.web.rest.errors.BadRequestAlertException;
import br.com.locadora.web.rest.util.HeaderUtil;
import br.com.locadora.web.rest.util.PaginationUtil;
import br.com.locadora.service.dto.DiretorDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Diretor.
 */
@RestController
@RequestMapping("/api")
public class DiretorResource {

    private final Logger log = LoggerFactory.getLogger(DiretorResource.class);

    private static final String ENTITY_NAME = "diretor";

    private final DiretorService diretorService;

    public DiretorResource(DiretorService diretorService) {
        this.diretorService = diretorService;
    }

    /**
     * POST  /diretors : Create a new diretor.
     *
     * @param diretorDTO the diretorDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new diretorDTO, or with status 400 (Bad Request) if the diretor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/diretors")
    public ResponseEntity<DiretorDTO> createDiretor(@Valid @RequestBody DiretorDTO diretorDTO) throws URISyntaxException {
        log.debug("REST request to save Diretor : {}", diretorDTO);
        if (diretorDTO.getId() != null) {
            throw new BadRequestAlertException("A new diretor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DiretorDTO result = diretorService.save(diretorDTO);
        return ResponseEntity.created(new URI("/api/diretors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /diretors : Updates an existing diretor.
     *
     * @param diretorDTO the diretorDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated diretorDTO,
     * or with status 400 (Bad Request) if the diretorDTO is not valid,
     * or with status 500 (Internal Server Error) if the diretorDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/diretors")
    public ResponseEntity<DiretorDTO> updateDiretor(@Valid @RequestBody DiretorDTO diretorDTO) throws URISyntaxException {
        log.debug("REST request to update Diretor : {}", diretorDTO);
        if (diretorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DiretorDTO result = diretorService.save(diretorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, diretorDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /diretors : get all the diretors.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of diretors in body
     */
    @GetMapping("/diretors")
    public ResponseEntity<List<DiretorDTO>> getAllDiretors(Pageable pageable) {
        log.debug("REST request to get a page of Diretors");
        Page<DiretorDTO> page = diretorService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/diretors");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /diretors/:id : get the "id" diretor.
     *
     * @param id the id of the diretorDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the diretorDTO, or with status 404 (Not Found)
     */
    @GetMapping("/diretors/{id}")
    public ResponseEntity<DiretorDTO> getDiretor(@PathVariable Long id) {
        log.debug("REST request to get Diretor : {}", id);
        Optional<DiretorDTO> diretorDTO = diretorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(diretorDTO);
    }

    /**
     * DELETE  /diretors/:id : delete the "id" diretor.
     *
     * @param id the id of the diretorDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/diretors/{id}")
    public ResponseEntity<Void> deleteDiretor(@PathVariable Long id) {
        log.debug("REST request to delete Diretor : {}", id);
        diretorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
