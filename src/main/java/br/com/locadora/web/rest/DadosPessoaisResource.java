package br.com.locadora.web.rest;
import br.com.locadora.service.DadosPessoaisService;
import br.com.locadora.web.rest.errors.BadRequestAlertException;
import br.com.locadora.web.rest.util.HeaderUtil;
import br.com.locadora.web.rest.util.PaginationUtil;
import br.com.locadora.service.dto.DadosPessoaisDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DadosPessoais.
 */
@RestController
@RequestMapping("/api")
public class DadosPessoaisResource {

    private final Logger log = LoggerFactory.getLogger(DadosPessoaisResource.class);

    private static final String ENTITY_NAME = "dadosPessoais";

    private final DadosPessoaisService dadosPessoaisService;

    public DadosPessoaisResource(DadosPessoaisService dadosPessoaisService) {
        this.dadosPessoaisService = dadosPessoaisService;
    }

    /**
     * POST  /dados-pessoais : Create a new dadosPessoais.
     *
     * @param dadosPessoaisDTO the dadosPessoaisDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dadosPessoaisDTO, or with status 400 (Bad Request) if the dadosPessoais has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dados-pessoais")
    public ResponseEntity<DadosPessoaisDTO> createDadosPessoais(@Valid @RequestBody DadosPessoaisDTO dadosPessoaisDTO) throws URISyntaxException {
        log.debug("REST request to save DadosPessoais : {}", dadosPessoaisDTO);
        if (dadosPessoaisDTO.getId() != null) {
            throw new BadRequestAlertException("A new dadosPessoais cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DadosPessoaisDTO result = dadosPessoaisService.save(dadosPessoaisDTO);
        return ResponseEntity.created(new URI("/api/dados-pessoais/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dados-pessoais : Updates an existing dadosPessoais.
     *
     * @param dadosPessoaisDTO the dadosPessoaisDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dadosPessoaisDTO,
     * or with status 400 (Bad Request) if the dadosPessoaisDTO is not valid,
     * or with status 500 (Internal Server Error) if the dadosPessoaisDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dados-pessoais")
    public ResponseEntity<DadosPessoaisDTO> updateDadosPessoais(@Valid @RequestBody DadosPessoaisDTO dadosPessoaisDTO) throws URISyntaxException {
        log.debug("REST request to update DadosPessoais : {}", dadosPessoaisDTO);
        if (dadosPessoaisDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DadosPessoaisDTO result = dadosPessoaisService.save(dadosPessoaisDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dadosPessoaisDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dados-pessoais : get all the dadosPessoais.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dadosPessoais in body
     */
    @GetMapping("/dados-pessoais")
    public ResponseEntity<List<DadosPessoaisDTO>> getAllDadosPessoais(Pageable pageable) {
        log.debug("REST request to get a page of DadosPessoais");
        Page<DadosPessoaisDTO> page = dadosPessoaisService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dados-pessoais");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /dados-pessoais/:id : get the "id" dadosPessoais.
     *
     * @param id the id of the dadosPessoaisDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dadosPessoaisDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dados-pessoais/{id}")
    public ResponseEntity<DadosPessoaisDTO> getDadosPessoais(@PathVariable Long id) {
        log.debug("REST request to get DadosPessoais : {}", id);
        Optional<DadosPessoaisDTO> dadosPessoaisDTO = dadosPessoaisService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dadosPessoaisDTO);
    }

    /**
     * DELETE  /dados-pessoais/:id : delete the "id" dadosPessoais.
     *
     * @param id the id of the dadosPessoaisDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dados-pessoais/{id}")
    public ResponseEntity<Void> deleteDadosPessoais(@PathVariable Long id) {
        log.debug("REST request to delete DadosPessoais : {}", id);
        dadosPessoaisService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
