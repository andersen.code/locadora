package br.com.locadora.web.rest;

import br.com.locadora.LocadoraApp;

import br.com.locadora.domain.Locacao;
import br.com.locadora.repository.LocacaoRepository;
import br.com.locadora.service.LocacaoService;
import br.com.locadora.service.dto.LocacaoDTO;
import br.com.locadora.service.mapper.LocacaoMapper;
import br.com.locadora.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static br.com.locadora.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LocacaoResource REST controller.
 *
 * @see LocacaoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LocadoraApp.class)
public class LocacaoResourceIntTest {

    private static final LocalDate DEFAULT_DATA_LOCACAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_LOCACAO = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_VALOR_LOCACAO = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALOR_LOCACAO = new BigDecimal(2);

    private static final String DEFAULT_OBSERVACAO = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACAO = "BBBBBBBBBB";

    @Autowired
    private LocacaoRepository locacaoRepository;

    @Autowired
    private LocacaoMapper locacaoMapper;

    @Autowired
    private LocacaoService locacaoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLocacaoMockMvc;

    private Locacao locacao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LocacaoResource locacaoResource = new LocacaoResource(locacaoService);
        this.restLocacaoMockMvc = MockMvcBuilders.standaloneSetup(locacaoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Locacao createEntity(EntityManager em) {
        Locacao locacao = new Locacao()
            .dataLocacao(DEFAULT_DATA_LOCACAO)
            .valorLocacao(DEFAULT_VALOR_LOCACAO)
            .observacao(DEFAULT_OBSERVACAO);
        return locacao;
    }

    @Before
    public void initTest() {
        locacao = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocacao() throws Exception {
        int databaseSizeBeforeCreate = locacaoRepository.findAll().size();

        // Create the Locacao
        LocacaoDTO locacaoDTO = locacaoMapper.toDto(locacao);
        restLocacaoMockMvc.perform(post("/api/locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(locacaoDTO)))
            .andExpect(status().isCreated());

        // Validate the Locacao in the database
        List<Locacao> locacaoList = locacaoRepository.findAll();
        assertThat(locacaoList).hasSize(databaseSizeBeforeCreate + 1);
        Locacao testLocacao = locacaoList.get(locacaoList.size() - 1);
        assertThat(testLocacao.getDataLocacao()).isEqualTo(DEFAULT_DATA_LOCACAO);
        assertThat(testLocacao.getValorLocacao()).isEqualTo(DEFAULT_VALOR_LOCACAO);
        assertThat(testLocacao.getObservacao()).isEqualTo(DEFAULT_OBSERVACAO);
    }

    @Test
    @Transactional
    public void createLocacaoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locacaoRepository.findAll().size();

        // Create the Locacao with an existing ID
        locacao.setId(1L);
        LocacaoDTO locacaoDTO = locacaoMapper.toDto(locacao);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocacaoMockMvc.perform(post("/api/locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(locacaoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Locacao in the database
        List<Locacao> locacaoList = locacaoRepository.findAll();
        assertThat(locacaoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDataLocacaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = locacaoRepository.findAll().size();
        // set the field null
        locacao.setDataLocacao(null);

        // Create the Locacao, which fails.
        LocacaoDTO locacaoDTO = locacaoMapper.toDto(locacao);

        restLocacaoMockMvc.perform(post("/api/locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(locacaoDTO)))
            .andExpect(status().isBadRequest());

        List<Locacao> locacaoList = locacaoRepository.findAll();
        assertThat(locacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocacaos() throws Exception {
        // Initialize the database
        locacaoRepository.saveAndFlush(locacao);

        // Get all the locacaoList
        restLocacaoMockMvc.perform(get("/api/locacaos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataLocacao").value(hasItem(DEFAULT_DATA_LOCACAO.toString())))
            .andExpect(jsonPath("$.[*].valorLocacao").value(hasItem(DEFAULT_VALOR_LOCACAO.intValue())))
            .andExpect(jsonPath("$.[*].observacao").value(hasItem(DEFAULT_OBSERVACAO.toString())));
    }
    
    @Test
    @Transactional
    public void getLocacao() throws Exception {
        // Initialize the database
        locacaoRepository.saveAndFlush(locacao);

        // Get the locacao
        restLocacaoMockMvc.perform(get("/api/locacaos/{id}", locacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(locacao.getId().intValue()))
            .andExpect(jsonPath("$.dataLocacao").value(DEFAULT_DATA_LOCACAO.toString()))
            .andExpect(jsonPath("$.valorLocacao").value(DEFAULT_VALOR_LOCACAO.intValue()))
            .andExpect(jsonPath("$.observacao").value(DEFAULT_OBSERVACAO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLocacao() throws Exception {
        // Get the locacao
        restLocacaoMockMvc.perform(get("/api/locacaos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocacao() throws Exception {
        // Initialize the database
        locacaoRepository.saveAndFlush(locacao);

        int databaseSizeBeforeUpdate = locacaoRepository.findAll().size();

        // Update the locacao
        Locacao updatedLocacao = locacaoRepository.findById(locacao.getId()).get();
        // Disconnect from session so that the updates on updatedLocacao are not directly saved in db
        em.detach(updatedLocacao);
        updatedLocacao
            .dataLocacao(UPDATED_DATA_LOCACAO)
            .valorLocacao(UPDATED_VALOR_LOCACAO)
            .observacao(UPDATED_OBSERVACAO);
        LocacaoDTO locacaoDTO = locacaoMapper.toDto(updatedLocacao);

        restLocacaoMockMvc.perform(put("/api/locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(locacaoDTO)))
            .andExpect(status().isOk());

        // Validate the Locacao in the database
        List<Locacao> locacaoList = locacaoRepository.findAll();
        assertThat(locacaoList).hasSize(databaseSizeBeforeUpdate);
        Locacao testLocacao = locacaoList.get(locacaoList.size() - 1);
        assertThat(testLocacao.getDataLocacao()).isEqualTo(UPDATED_DATA_LOCACAO);
        assertThat(testLocacao.getValorLocacao()).isEqualTo(UPDATED_VALOR_LOCACAO);
        assertThat(testLocacao.getObservacao()).isEqualTo(UPDATED_OBSERVACAO);
    }

    @Test
    @Transactional
    public void updateNonExistingLocacao() throws Exception {
        int databaseSizeBeforeUpdate = locacaoRepository.findAll().size();

        // Create the Locacao
        LocacaoDTO locacaoDTO = locacaoMapper.toDto(locacao);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocacaoMockMvc.perform(put("/api/locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(locacaoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Locacao in the database
        List<Locacao> locacaoList = locacaoRepository.findAll();
        assertThat(locacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocacao() throws Exception {
        // Initialize the database
        locacaoRepository.saveAndFlush(locacao);

        int databaseSizeBeforeDelete = locacaoRepository.findAll().size();

        // Delete the locacao
        restLocacaoMockMvc.perform(delete("/api/locacaos/{id}", locacao.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Locacao> locacaoList = locacaoRepository.findAll();
        assertThat(locacaoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Locacao.class);
        Locacao locacao1 = new Locacao();
        locacao1.setId(1L);
        Locacao locacao2 = new Locacao();
        locacao2.setId(locacao1.getId());
        assertThat(locacao1).isEqualTo(locacao2);
        locacao2.setId(2L);
        assertThat(locacao1).isNotEqualTo(locacao2);
        locacao1.setId(null);
        assertThat(locacao1).isNotEqualTo(locacao2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocacaoDTO.class);
        LocacaoDTO locacaoDTO1 = new LocacaoDTO();
        locacaoDTO1.setId(1L);
        LocacaoDTO locacaoDTO2 = new LocacaoDTO();
        assertThat(locacaoDTO1).isNotEqualTo(locacaoDTO2);
        locacaoDTO2.setId(locacaoDTO1.getId());
        assertThat(locacaoDTO1).isEqualTo(locacaoDTO2);
        locacaoDTO2.setId(2L);
        assertThat(locacaoDTO1).isNotEqualTo(locacaoDTO2);
        locacaoDTO1.setId(null);
        assertThat(locacaoDTO1).isNotEqualTo(locacaoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(locacaoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(locacaoMapper.fromId(null)).isNull();
    }
}
