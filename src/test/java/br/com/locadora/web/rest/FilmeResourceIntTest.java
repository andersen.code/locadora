package br.com.locadora.web.rest;

import br.com.locadora.LocadoraApp;

import br.com.locadora.domain.Filme;
import br.com.locadora.repository.FilmeRepository;
import br.com.locadora.service.FilmeService;
import br.com.locadora.service.dto.FilmeDTO;
import br.com.locadora.service.mapper.FilmeMapper;
import br.com.locadora.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.locadora.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.locadora.domain.enumeration.Categoria;
/**
 * Test class for the FilmeResource REST controller.
 *
 * @see FilmeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LocadoraApp.class)
public class FilmeResourceIntTest {

    private static final String DEFAULT_TITULO = "AAAAAAAAAA";
    private static final String UPDATED_TITULO = "BBBBBBBBBB";

    private static final Categoria DEFAULT_CATEGORIA = Categoria.COMEDIA;
    private static final Categoria UPDATED_CATEGORIA = Categoria.ACAO;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private FilmeMapper filmeMapper;

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFilmeMockMvc;

    private Filme filme;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FilmeResource filmeResource = new FilmeResource(filmeService);
        this.restFilmeMockMvc = MockMvcBuilders.standaloneSetup(filmeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Filme createEntity(EntityManager em) {
        Filme filme = new Filme()
            .titulo(DEFAULT_TITULO)
            .categoria(DEFAULT_CATEGORIA);
        return filme;
    }

    @Before
    public void initTest() {
        filme = createEntity(em);
    }

    @Test
    @Transactional
    public void createFilme() throws Exception {
        int databaseSizeBeforeCreate = filmeRepository.findAll().size();

        // Create the Filme
        FilmeDTO filmeDTO = filmeMapper.toDto(filme);
        restFilmeMockMvc.perform(post("/api/filmes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmeDTO)))
            .andExpect(status().isCreated());

        // Validate the Filme in the database
        List<Filme> filmeList = filmeRepository.findAll();
        assertThat(filmeList).hasSize(databaseSizeBeforeCreate + 1);
        Filme testFilme = filmeList.get(filmeList.size() - 1);
        assertThat(testFilme.getTitulo()).isEqualTo(DEFAULT_TITULO);
        assertThat(testFilme.getCategoria()).isEqualTo(DEFAULT_CATEGORIA);
    }

    @Test
    @Transactional
    public void createFilmeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = filmeRepository.findAll().size();

        // Create the Filme with an existing ID
        filme.setId(1L);
        FilmeDTO filmeDTO = filmeMapper.toDto(filme);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFilmeMockMvc.perform(post("/api/filmes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Filme in the database
        List<Filme> filmeList = filmeRepository.findAll();
        assertThat(filmeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTituloIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmeRepository.findAll().size();
        // set the field null
        filme.setTitulo(null);

        // Create the Filme, which fails.
        FilmeDTO filmeDTO = filmeMapper.toDto(filme);

        restFilmeMockMvc.perform(post("/api/filmes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmeDTO)))
            .andExpect(status().isBadRequest());

        List<Filme> filmeList = filmeRepository.findAll();
        assertThat(filmeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFilmes() throws Exception {
        // Initialize the database
        filmeRepository.saveAndFlush(filme);

        // Get all the filmeList
        restFilmeMockMvc.perform(get("/api/filmes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(filme.getId().intValue())))
            .andExpect(jsonPath("$.[*].titulo").value(hasItem(DEFAULT_TITULO.toString())))
            .andExpect(jsonPath("$.[*].categoria").value(hasItem(DEFAULT_CATEGORIA.toString())));
    }
    
    @Test
    @Transactional
    public void getFilme() throws Exception {
        // Initialize the database
        filmeRepository.saveAndFlush(filme);

        // Get the filme
        restFilmeMockMvc.perform(get("/api/filmes/{id}", filme.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(filme.getId().intValue()))
            .andExpect(jsonPath("$.titulo").value(DEFAULT_TITULO.toString()))
            .andExpect(jsonPath("$.categoria").value(DEFAULT_CATEGORIA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFilme() throws Exception {
        // Get the filme
        restFilmeMockMvc.perform(get("/api/filmes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFilme() throws Exception {
        // Initialize the database
        filmeRepository.saveAndFlush(filme);

        int databaseSizeBeforeUpdate = filmeRepository.findAll().size();

        // Update the filme
        Filme updatedFilme = filmeRepository.findById(filme.getId()).get();
        // Disconnect from session so that the updates on updatedFilme are not directly saved in db
        em.detach(updatedFilme);
        updatedFilme
            .titulo(UPDATED_TITULO)
            .categoria(UPDATED_CATEGORIA);
        FilmeDTO filmeDTO = filmeMapper.toDto(updatedFilme);

        restFilmeMockMvc.perform(put("/api/filmes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmeDTO)))
            .andExpect(status().isOk());

        // Validate the Filme in the database
        List<Filme> filmeList = filmeRepository.findAll();
        assertThat(filmeList).hasSize(databaseSizeBeforeUpdate);
        Filme testFilme = filmeList.get(filmeList.size() - 1);
        assertThat(testFilme.getTitulo()).isEqualTo(UPDATED_TITULO);
        assertThat(testFilme.getCategoria()).isEqualTo(UPDATED_CATEGORIA);
    }

    @Test
    @Transactional
    public void updateNonExistingFilme() throws Exception {
        int databaseSizeBeforeUpdate = filmeRepository.findAll().size();

        // Create the Filme
        FilmeDTO filmeDTO = filmeMapper.toDto(filme);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFilmeMockMvc.perform(put("/api/filmes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Filme in the database
        List<Filme> filmeList = filmeRepository.findAll();
        assertThat(filmeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFilme() throws Exception {
        // Initialize the database
        filmeRepository.saveAndFlush(filme);

        int databaseSizeBeforeDelete = filmeRepository.findAll().size();

        // Delete the filme
        restFilmeMockMvc.perform(delete("/api/filmes/{id}", filme.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Filme> filmeList = filmeRepository.findAll();
        assertThat(filmeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Filme.class);
        Filme filme1 = new Filme();
        filme1.setId(1L);
        Filme filme2 = new Filme();
        filme2.setId(filme1.getId());
        assertThat(filme1).isEqualTo(filme2);
        filme2.setId(2L);
        assertThat(filme1).isNotEqualTo(filme2);
        filme1.setId(null);
        assertThat(filme1).isNotEqualTo(filme2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FilmeDTO.class);
        FilmeDTO filmeDTO1 = new FilmeDTO();
        filmeDTO1.setId(1L);
        FilmeDTO filmeDTO2 = new FilmeDTO();
        assertThat(filmeDTO1).isNotEqualTo(filmeDTO2);
        filmeDTO2.setId(filmeDTO1.getId());
        assertThat(filmeDTO1).isEqualTo(filmeDTO2);
        filmeDTO2.setId(2L);
        assertThat(filmeDTO1).isNotEqualTo(filmeDTO2);
        filmeDTO1.setId(null);
        assertThat(filmeDTO1).isNotEqualTo(filmeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(filmeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(filmeMapper.fromId(null)).isNull();
    }
}
