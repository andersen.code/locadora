package br.com.locadora.web.rest;

import br.com.locadora.LocadoraApp;

import br.com.locadora.domain.DadosPessoais;
import br.com.locadora.domain.User;
import br.com.locadora.repository.DadosPessoaisRepository;
import br.com.locadora.service.DadosPessoaisService;
import br.com.locadora.service.dto.DadosPessoaisDTO;
import br.com.locadora.service.mapper.DadosPessoaisMapper;
import br.com.locadora.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.locadora.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.locadora.domain.enumeration.TipoPessoa;
import br.com.locadora.domain.enumeration.ClienteSituacao;
/**
 * Test class for the DadosPessoaisResource REST controller.
 *
 * @see DadosPessoaisResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LocadoraApp.class)
public class DadosPessoaisResourceIntTest {

    private static final TipoPessoa DEFAULT_TIPO_PESSOA = TipoPessoa.FISICA;
    private static final TipoPessoa UPDATED_TIPO_PESSOA = TipoPessoa.JURIDICA;

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_DOCUMENTO_RECEITA_FEDERAL = "AAAAAAAAAAA";
    private static final String UPDATED_DOCUMENTO_RECEITA_FEDERAL = "BBBBBBBBBBB";

    private static final ClienteSituacao DEFAULT_SITUACAO = ClienteSituacao.ATIVO;
    private static final ClienteSituacao UPDATED_SITUACAO = ClienteSituacao.INATIVO;

    private static final String DEFAULT_OBSERVACAO = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACAO = "BBBBBBBBBB";

    @Autowired
    private DadosPessoaisRepository dadosPessoaisRepository;

    @Autowired
    private DadosPessoaisMapper dadosPessoaisMapper;

    @Autowired
    private DadosPessoaisService dadosPessoaisService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDadosPessoaisMockMvc;

    private DadosPessoais dadosPessoais;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DadosPessoaisResource dadosPessoaisResource = new DadosPessoaisResource(dadosPessoaisService);
        this.restDadosPessoaisMockMvc = MockMvcBuilders.standaloneSetup(dadosPessoaisResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DadosPessoais createEntity(EntityManager em) {
        DadosPessoais dadosPessoais = new DadosPessoais()
            .tipoPessoa(DEFAULT_TIPO_PESSOA)
            .nome(DEFAULT_NOME)
            .documentoReceitaFederal(DEFAULT_DOCUMENTO_RECEITA_FEDERAL)
            .situacao(DEFAULT_SITUACAO)
            .observacao(DEFAULT_OBSERVACAO);
        // Add required entity
        User user = UserResourceIntTest.createEntity(em);
        em.persist(user);
        em.flush();
        dadosPessoais.setUser(user);
        return dadosPessoais;
    }

    @Before
    public void initTest() {
        dadosPessoais = createEntity(em);
    }

    @Test
    @Transactional
    public void createDadosPessoais() throws Exception {
        int databaseSizeBeforeCreate = dadosPessoaisRepository.findAll().size();

        // Create the DadosPessoais
        DadosPessoaisDTO dadosPessoaisDTO = dadosPessoaisMapper.toDto(dadosPessoais);
        restDadosPessoaisMockMvc.perform(post("/api/dados-pessoais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dadosPessoaisDTO)))
            .andExpect(status().isCreated());

        // Validate the DadosPessoais in the database
        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeCreate + 1);
        DadosPessoais testDadosPessoais = dadosPessoaisList.get(dadosPessoaisList.size() - 1);
        assertThat(testDadosPessoais.getTipoPessoa()).isEqualTo(DEFAULT_TIPO_PESSOA);
        assertThat(testDadosPessoais.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testDadosPessoais.getDocumentoReceitaFederal()).isEqualTo(DEFAULT_DOCUMENTO_RECEITA_FEDERAL);
        assertThat(testDadosPessoais.getSituacao()).isEqualTo(DEFAULT_SITUACAO);
        assertThat(testDadosPessoais.getObservacao()).isEqualTo(DEFAULT_OBSERVACAO);
    }

    @Test
    @Transactional
    public void createDadosPessoaisWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dadosPessoaisRepository.findAll().size();

        // Create the DadosPessoais with an existing ID
        dadosPessoais.setId(1L);
        DadosPessoaisDTO dadosPessoaisDTO = dadosPessoaisMapper.toDto(dadosPessoais);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDadosPessoaisMockMvc.perform(post("/api/dados-pessoais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dadosPessoaisDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DadosPessoais in the database
        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTipoPessoaIsRequired() throws Exception {
        int databaseSizeBeforeTest = dadosPessoaisRepository.findAll().size();
        // set the field null
        dadosPessoais.setTipoPessoa(null);

        // Create the DadosPessoais, which fails.
        DadosPessoaisDTO dadosPessoaisDTO = dadosPessoaisMapper.toDto(dadosPessoais);

        restDadosPessoaisMockMvc.perform(post("/api/dados-pessoais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dadosPessoaisDTO)))
            .andExpect(status().isBadRequest());

        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dadosPessoaisRepository.findAll().size();
        // set the field null
        dadosPessoais.setNome(null);

        // Create the DadosPessoais, which fails.
        DadosPessoaisDTO dadosPessoaisDTO = dadosPessoaisMapper.toDto(dadosPessoais);

        restDadosPessoaisMockMvc.perform(post("/api/dados-pessoais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dadosPessoaisDTO)))
            .andExpect(status().isBadRequest());

        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDocumentoReceitaFederalIsRequired() throws Exception {
        int databaseSizeBeforeTest = dadosPessoaisRepository.findAll().size();
        // set the field null
        dadosPessoais.setDocumentoReceitaFederal(null);

        // Create the DadosPessoais, which fails.
        DadosPessoaisDTO dadosPessoaisDTO = dadosPessoaisMapper.toDto(dadosPessoais);

        restDadosPessoaisMockMvc.perform(post("/api/dados-pessoais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dadosPessoaisDTO)))
            .andExpect(status().isBadRequest());

        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDadosPessoais() throws Exception {
        // Initialize the database
        dadosPessoaisRepository.saveAndFlush(dadosPessoais);

        // Get all the dadosPessoaisList
        restDadosPessoaisMockMvc.perform(get("/api/dados-pessoais?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dadosPessoais.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoPessoa").value(hasItem(DEFAULT_TIPO_PESSOA.toString())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].documentoReceitaFederal").value(hasItem(DEFAULT_DOCUMENTO_RECEITA_FEDERAL.toString())))
            .andExpect(jsonPath("$.[*].situacao").value(hasItem(DEFAULT_SITUACAO.toString())))
            .andExpect(jsonPath("$.[*].observacao").value(hasItem(DEFAULT_OBSERVACAO.toString())));
    }
    
    @Test
    @Transactional
    public void getDadosPessoais() throws Exception {
        // Initialize the database
        dadosPessoaisRepository.saveAndFlush(dadosPessoais);

        // Get the dadosPessoais
        restDadosPessoaisMockMvc.perform(get("/api/dados-pessoais/{id}", dadosPessoais.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dadosPessoais.getId().intValue()))
            .andExpect(jsonPath("$.tipoPessoa").value(DEFAULT_TIPO_PESSOA.toString()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.documentoReceitaFederal").value(DEFAULT_DOCUMENTO_RECEITA_FEDERAL.toString()))
            .andExpect(jsonPath("$.situacao").value(DEFAULT_SITUACAO.toString()))
            .andExpect(jsonPath("$.observacao").value(DEFAULT_OBSERVACAO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDadosPessoais() throws Exception {
        // Get the dadosPessoais
        restDadosPessoaisMockMvc.perform(get("/api/dados-pessoais/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDadosPessoais() throws Exception {
        // Initialize the database
        dadosPessoaisRepository.saveAndFlush(dadosPessoais);

        int databaseSizeBeforeUpdate = dadosPessoaisRepository.findAll().size();

        // Update the dadosPessoais
        DadosPessoais updatedDadosPessoais = dadosPessoaisRepository.findById(dadosPessoais.getId()).get();
        // Disconnect from session so that the updates on updatedDadosPessoais are not directly saved in db
        em.detach(updatedDadosPessoais);
        updatedDadosPessoais
            .tipoPessoa(UPDATED_TIPO_PESSOA)
            .nome(UPDATED_NOME)
            .documentoReceitaFederal(UPDATED_DOCUMENTO_RECEITA_FEDERAL)
            .situacao(UPDATED_SITUACAO)
            .observacao(UPDATED_OBSERVACAO);
        DadosPessoaisDTO dadosPessoaisDTO = dadosPessoaisMapper.toDto(updatedDadosPessoais);

        restDadosPessoaisMockMvc.perform(put("/api/dados-pessoais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dadosPessoaisDTO)))
            .andExpect(status().isOk());

        // Validate the DadosPessoais in the database
        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeUpdate);
        DadosPessoais testDadosPessoais = dadosPessoaisList.get(dadosPessoaisList.size() - 1);
        assertThat(testDadosPessoais.getTipoPessoa()).isEqualTo(UPDATED_TIPO_PESSOA);
        assertThat(testDadosPessoais.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testDadosPessoais.getDocumentoReceitaFederal()).isEqualTo(UPDATED_DOCUMENTO_RECEITA_FEDERAL);
        assertThat(testDadosPessoais.getSituacao()).isEqualTo(UPDATED_SITUACAO);
        assertThat(testDadosPessoais.getObservacao()).isEqualTo(UPDATED_OBSERVACAO);
    }

    @Test
    @Transactional
    public void updateNonExistingDadosPessoais() throws Exception {
        int databaseSizeBeforeUpdate = dadosPessoaisRepository.findAll().size();

        // Create the DadosPessoais
        DadosPessoaisDTO dadosPessoaisDTO = dadosPessoaisMapper.toDto(dadosPessoais);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDadosPessoaisMockMvc.perform(put("/api/dados-pessoais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dadosPessoaisDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DadosPessoais in the database
        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDadosPessoais() throws Exception {
        // Initialize the database
        dadosPessoaisRepository.saveAndFlush(dadosPessoais);

        int databaseSizeBeforeDelete = dadosPessoaisRepository.findAll().size();

        // Delete the dadosPessoais
        restDadosPessoaisMockMvc.perform(delete("/api/dados-pessoais/{id}", dadosPessoais.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DadosPessoais> dadosPessoaisList = dadosPessoaisRepository.findAll();
        assertThat(dadosPessoaisList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DadosPessoais.class);
        DadosPessoais dadosPessoais1 = new DadosPessoais();
        dadosPessoais1.setId(1L);
        DadosPessoais dadosPessoais2 = new DadosPessoais();
        dadosPessoais2.setId(dadosPessoais1.getId());
        assertThat(dadosPessoais1).isEqualTo(dadosPessoais2);
        dadosPessoais2.setId(2L);
        assertThat(dadosPessoais1).isNotEqualTo(dadosPessoais2);
        dadosPessoais1.setId(null);
        assertThat(dadosPessoais1).isNotEqualTo(dadosPessoais2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DadosPessoaisDTO.class);
        DadosPessoaisDTO dadosPessoaisDTO1 = new DadosPessoaisDTO();
        dadosPessoaisDTO1.setId(1L);
        DadosPessoaisDTO dadosPessoaisDTO2 = new DadosPessoaisDTO();
        assertThat(dadosPessoaisDTO1).isNotEqualTo(dadosPessoaisDTO2);
        dadosPessoaisDTO2.setId(dadosPessoaisDTO1.getId());
        assertThat(dadosPessoaisDTO1).isEqualTo(dadosPessoaisDTO2);
        dadosPessoaisDTO2.setId(2L);
        assertThat(dadosPessoaisDTO1).isNotEqualTo(dadosPessoaisDTO2);
        dadosPessoaisDTO1.setId(null);
        assertThat(dadosPessoaisDTO1).isNotEqualTo(dadosPessoaisDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(dadosPessoaisMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dadosPessoaisMapper.fromId(null)).isNull();
    }
}
