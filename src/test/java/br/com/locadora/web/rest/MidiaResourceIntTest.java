package br.com.locadora.web.rest;

import br.com.locadora.LocadoraApp;

import br.com.locadora.domain.Midia;
import br.com.locadora.repository.MidiaRepository;
import br.com.locadora.service.MidiaService;
import br.com.locadora.service.dto.MidiaDTO;
import br.com.locadora.service.mapper.MidiaMapper;
import br.com.locadora.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static br.com.locadora.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.locadora.domain.enumeration.IdiomaFilme;
import br.com.locadora.domain.enumeration.TipoMidia;
import br.com.locadora.domain.enumeration.StatusMidia;
/**
 * Test class for the MidiaResource REST controller.
 *
 * @see MidiaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LocadoraApp.class)
public class MidiaResourceIntTest {

    private static final IdiomaFilme DEFAULT_IDIOMA = IdiomaFilme.PORTUGUES;
    private static final IdiomaFilme UPDATED_IDIOMA = IdiomaFilme.ESPANHOL;

    private static final TipoMidia DEFAULT_TIPO = TipoMidia.DVD;
    private static final TipoMidia UPDATED_TIPO = TipoMidia.CD;

    private static final StatusMidia DEFAULT_STATUS_MIDIA = StatusMidia.LOCADA;
    private static final StatusMidia UPDATED_STATUS_MIDIA = StatusMidia.RESERVADA;

    @Autowired
    private MidiaRepository midiaRepository;

    @Mock
    private MidiaRepository midiaRepositoryMock;

    @Autowired
    private MidiaMapper midiaMapper;

    @Mock
    private MidiaService midiaServiceMock;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMidiaMockMvc;

    private Midia midia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MidiaResource midiaResource = new MidiaResource(midiaService);
        this.restMidiaMockMvc = MockMvcBuilders.standaloneSetup(midiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Midia createEntity(EntityManager em) {
        Midia midia = new Midia()
            .idioma(DEFAULT_IDIOMA)
            .tipo(DEFAULT_TIPO)
            .statusMidia(DEFAULT_STATUS_MIDIA);
        return midia;
    }

    @Before
    public void initTest() {
        midia = createEntity(em);
    }

    @Test
    @Transactional
    public void createMidia() throws Exception {
        int databaseSizeBeforeCreate = midiaRepository.findAll().size();

        // Create the Midia
        MidiaDTO midiaDTO = midiaMapper.toDto(midia);
        restMidiaMockMvc.perform(post("/api/midias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(midiaDTO)))
            .andExpect(status().isCreated());

        // Validate the Midia in the database
        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeCreate + 1);
        Midia testMidia = midiaList.get(midiaList.size() - 1);
        assertThat(testMidia.getIdioma()).isEqualTo(DEFAULT_IDIOMA);
        assertThat(testMidia.getTipo()).isEqualTo(DEFAULT_TIPO);
        assertThat(testMidia.getStatusMidia()).isEqualTo(DEFAULT_STATUS_MIDIA);
    }

    @Test
    @Transactional
    public void createMidiaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = midiaRepository.findAll().size();

        // Create the Midia with an existing ID
        midia.setId(1L);
        MidiaDTO midiaDTO = midiaMapper.toDto(midia);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMidiaMockMvc.perform(post("/api/midias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(midiaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Midia in the database
        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIdiomaIsRequired() throws Exception {
        int databaseSizeBeforeTest = midiaRepository.findAll().size();
        // set the field null
        midia.setIdioma(null);

        // Create the Midia, which fails.
        MidiaDTO midiaDTO = midiaMapper.toDto(midia);

        restMidiaMockMvc.perform(post("/api/midias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(midiaDTO)))
            .andExpect(status().isBadRequest());

        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoIsRequired() throws Exception {
        int databaseSizeBeforeTest = midiaRepository.findAll().size();
        // set the field null
        midia.setTipo(null);

        // Create the Midia, which fails.
        MidiaDTO midiaDTO = midiaMapper.toDto(midia);

        restMidiaMockMvc.perform(post("/api/midias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(midiaDTO)))
            .andExpect(status().isBadRequest());

        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusMidiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = midiaRepository.findAll().size();
        // set the field null
        midia.setStatusMidia(null);

        // Create the Midia, which fails.
        MidiaDTO midiaDTO = midiaMapper.toDto(midia);

        restMidiaMockMvc.perform(post("/api/midias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(midiaDTO)))
            .andExpect(status().isBadRequest());

        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMidias() throws Exception {
        // Initialize the database
        midiaRepository.saveAndFlush(midia);

        // Get all the midiaList
        restMidiaMockMvc.perform(get("/api/midias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(midia.getId().intValue())))
            .andExpect(jsonPath("$.[*].idioma").value(hasItem(DEFAULT_IDIOMA.toString())))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())))
            .andExpect(jsonPath("$.[*].statusMidia").value(hasItem(DEFAULT_STATUS_MIDIA.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllMidiasWithEagerRelationshipsIsEnabled() throws Exception {
        MidiaResource midiaResource = new MidiaResource(midiaServiceMock);
        when(midiaServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restMidiaMockMvc = MockMvcBuilders.standaloneSetup(midiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restMidiaMockMvc.perform(get("/api/midias?eagerload=true"))
        .andExpect(status().isOk());

        verify(midiaServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllMidiasWithEagerRelationshipsIsNotEnabled() throws Exception {
        MidiaResource midiaResource = new MidiaResource(midiaServiceMock);
            when(midiaServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restMidiaMockMvc = MockMvcBuilders.standaloneSetup(midiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restMidiaMockMvc.perform(get("/api/midias?eagerload=true"))
        .andExpect(status().isOk());

            verify(midiaServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getMidia() throws Exception {
        // Initialize the database
        midiaRepository.saveAndFlush(midia);

        // Get the midia
        restMidiaMockMvc.perform(get("/api/midias/{id}", midia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(midia.getId().intValue()))
            .andExpect(jsonPath("$.idioma").value(DEFAULT_IDIOMA.toString()))
            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()))
            .andExpect(jsonPath("$.statusMidia").value(DEFAULT_STATUS_MIDIA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMidia() throws Exception {
        // Get the midia
        restMidiaMockMvc.perform(get("/api/midias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMidia() throws Exception {
        // Initialize the database
        midiaRepository.saveAndFlush(midia);

        int databaseSizeBeforeUpdate = midiaRepository.findAll().size();

        // Update the midia
        Midia updatedMidia = midiaRepository.findById(midia.getId()).get();
        // Disconnect from session so that the updates on updatedMidia are not directly saved in db
        em.detach(updatedMidia);
        updatedMidia
            .idioma(UPDATED_IDIOMA)
            .tipo(UPDATED_TIPO)
            .statusMidia(UPDATED_STATUS_MIDIA);
        MidiaDTO midiaDTO = midiaMapper.toDto(updatedMidia);

        restMidiaMockMvc.perform(put("/api/midias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(midiaDTO)))
            .andExpect(status().isOk());

        // Validate the Midia in the database
        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeUpdate);
        Midia testMidia = midiaList.get(midiaList.size() - 1);
        assertThat(testMidia.getIdioma()).isEqualTo(UPDATED_IDIOMA);
        assertThat(testMidia.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testMidia.getStatusMidia()).isEqualTo(UPDATED_STATUS_MIDIA);
    }

    @Test
    @Transactional
    public void updateNonExistingMidia() throws Exception {
        int databaseSizeBeforeUpdate = midiaRepository.findAll().size();

        // Create the Midia
        MidiaDTO midiaDTO = midiaMapper.toDto(midia);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMidiaMockMvc.perform(put("/api/midias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(midiaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Midia in the database
        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMidia() throws Exception {
        // Initialize the database
        midiaRepository.saveAndFlush(midia);

        int databaseSizeBeforeDelete = midiaRepository.findAll().size();

        // Delete the midia
        restMidiaMockMvc.perform(delete("/api/midias/{id}", midia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Midia.class);
        Midia midia1 = new Midia();
        midia1.setId(1L);
        Midia midia2 = new Midia();
        midia2.setId(midia1.getId());
        assertThat(midia1).isEqualTo(midia2);
        midia2.setId(2L);
        assertThat(midia1).isNotEqualTo(midia2);
        midia1.setId(null);
        assertThat(midia1).isNotEqualTo(midia2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MidiaDTO.class);
        MidiaDTO midiaDTO1 = new MidiaDTO();
        midiaDTO1.setId(1L);
        MidiaDTO midiaDTO2 = new MidiaDTO();
        assertThat(midiaDTO1).isNotEqualTo(midiaDTO2);
        midiaDTO2.setId(midiaDTO1.getId());
        assertThat(midiaDTO1).isEqualTo(midiaDTO2);
        midiaDTO2.setId(2L);
        assertThat(midiaDTO1).isNotEqualTo(midiaDTO2);
        midiaDTO1.setId(null);
        assertThat(midiaDTO1).isNotEqualTo(midiaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(midiaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(midiaMapper.fromId(null)).isNull();
    }
}
