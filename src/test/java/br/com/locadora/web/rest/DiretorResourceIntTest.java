package br.com.locadora.web.rest;

import br.com.locadora.LocadoraApp;

import br.com.locadora.domain.Diretor;
import br.com.locadora.repository.DiretorRepository;
import br.com.locadora.service.DiretorService;
import br.com.locadora.service.dto.DiretorDTO;
import br.com.locadora.service.mapper.DiretorMapper;
import br.com.locadora.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.locadora.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DiretorResource REST controller.
 *
 * @see DiretorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LocadoraApp.class)
public class DiretorResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_OBSERVACAO = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACAO = "BBBBBBBBBB";

    @Autowired
    private DiretorRepository diretorRepository;

    @Autowired
    private DiretorMapper diretorMapper;

    @Autowired
    private DiretorService diretorService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDiretorMockMvc;

    private Diretor diretor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DiretorResource diretorResource = new DiretorResource(diretorService);
        this.restDiretorMockMvc = MockMvcBuilders.standaloneSetup(diretorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Diretor createEntity(EntityManager em) {
        Diretor diretor = new Diretor()
            .nome(DEFAULT_NOME)
            .observacao(DEFAULT_OBSERVACAO);
        return diretor;
    }

    @Before
    public void initTest() {
        diretor = createEntity(em);
    }

    @Test
    @Transactional
    public void createDiretor() throws Exception {
        int databaseSizeBeforeCreate = diretorRepository.findAll().size();

        // Create the Diretor
        DiretorDTO diretorDTO = diretorMapper.toDto(diretor);
        restDiretorMockMvc.perform(post("/api/diretors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diretorDTO)))
            .andExpect(status().isCreated());

        // Validate the Diretor in the database
        List<Diretor> diretorList = diretorRepository.findAll();
        assertThat(diretorList).hasSize(databaseSizeBeforeCreate + 1);
        Diretor testDiretor = diretorList.get(diretorList.size() - 1);
        assertThat(testDiretor.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testDiretor.getObservacao()).isEqualTo(DEFAULT_OBSERVACAO);
    }

    @Test
    @Transactional
    public void createDiretorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = diretorRepository.findAll().size();

        // Create the Diretor with an existing ID
        diretor.setId(1L);
        DiretorDTO diretorDTO = diretorMapper.toDto(diretor);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDiretorMockMvc.perform(post("/api/diretors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diretorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Diretor in the database
        List<Diretor> diretorList = diretorRepository.findAll();
        assertThat(diretorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = diretorRepository.findAll().size();
        // set the field null
        diretor.setNome(null);

        // Create the Diretor, which fails.
        DiretorDTO diretorDTO = diretorMapper.toDto(diretor);

        restDiretorMockMvc.perform(post("/api/diretors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diretorDTO)))
            .andExpect(status().isBadRequest());

        List<Diretor> diretorList = diretorRepository.findAll();
        assertThat(diretorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDiretors() throws Exception {
        // Initialize the database
        diretorRepository.saveAndFlush(diretor);

        // Get all the diretorList
        restDiretorMockMvc.perform(get("/api/diretors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(diretor.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].observacao").value(hasItem(DEFAULT_OBSERVACAO.toString())));
    }
    
    @Test
    @Transactional
    public void getDiretor() throws Exception {
        // Initialize the database
        diretorRepository.saveAndFlush(diretor);

        // Get the diretor
        restDiretorMockMvc.perform(get("/api/diretors/{id}", diretor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(diretor.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.observacao").value(DEFAULT_OBSERVACAO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDiretor() throws Exception {
        // Get the diretor
        restDiretorMockMvc.perform(get("/api/diretors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDiretor() throws Exception {
        // Initialize the database
        diretorRepository.saveAndFlush(diretor);

        int databaseSizeBeforeUpdate = diretorRepository.findAll().size();

        // Update the diretor
        Diretor updatedDiretor = diretorRepository.findById(diretor.getId()).get();
        // Disconnect from session so that the updates on updatedDiretor are not directly saved in db
        em.detach(updatedDiretor);
        updatedDiretor
            .nome(UPDATED_NOME)
            .observacao(UPDATED_OBSERVACAO);
        DiretorDTO diretorDTO = diretorMapper.toDto(updatedDiretor);

        restDiretorMockMvc.perform(put("/api/diretors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diretorDTO)))
            .andExpect(status().isOk());

        // Validate the Diretor in the database
        List<Diretor> diretorList = diretorRepository.findAll();
        assertThat(diretorList).hasSize(databaseSizeBeforeUpdate);
        Diretor testDiretor = diretorList.get(diretorList.size() - 1);
        assertThat(testDiretor.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testDiretor.getObservacao()).isEqualTo(UPDATED_OBSERVACAO);
    }

    @Test
    @Transactional
    public void updateNonExistingDiretor() throws Exception {
        int databaseSizeBeforeUpdate = diretorRepository.findAll().size();

        // Create the Diretor
        DiretorDTO diretorDTO = diretorMapper.toDto(diretor);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDiretorMockMvc.perform(put("/api/diretors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diretorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Diretor in the database
        List<Diretor> diretorList = diretorRepository.findAll();
        assertThat(diretorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDiretor() throws Exception {
        // Initialize the database
        diretorRepository.saveAndFlush(diretor);

        int databaseSizeBeforeDelete = diretorRepository.findAll().size();

        // Delete the diretor
        restDiretorMockMvc.perform(delete("/api/diretors/{id}", diretor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Diretor> diretorList = diretorRepository.findAll();
        assertThat(diretorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Diretor.class);
        Diretor diretor1 = new Diretor();
        diretor1.setId(1L);
        Diretor diretor2 = new Diretor();
        diretor2.setId(diretor1.getId());
        assertThat(diretor1).isEqualTo(diretor2);
        diretor2.setId(2L);
        assertThat(diretor1).isNotEqualTo(diretor2);
        diretor1.setId(null);
        assertThat(diretor1).isNotEqualTo(diretor2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DiretorDTO.class);
        DiretorDTO diretorDTO1 = new DiretorDTO();
        diretorDTO1.setId(1L);
        DiretorDTO diretorDTO2 = new DiretorDTO();
        assertThat(diretorDTO1).isNotEqualTo(diretorDTO2);
        diretorDTO2.setId(diretorDTO1.getId());
        assertThat(diretorDTO1).isEqualTo(diretorDTO2);
        diretorDTO2.setId(2L);
        assertThat(diretorDTO1).isNotEqualTo(diretorDTO2);
        diretorDTO1.setId(null);
        assertThat(diretorDTO1).isNotEqualTo(diretorDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(diretorMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(diretorMapper.fromId(null)).isNull();
    }
}
