package br.com.locadora.web.rest;

import br.com.locadora.LocadoraApp;

import br.com.locadora.domain.Ator;
import br.com.locadora.repository.AtorRepository;
import br.com.locadora.service.AtorService;
import br.com.locadora.service.dto.AtorDTO;
import br.com.locadora.service.mapper.AtorMapper;
import br.com.locadora.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static br.com.locadora.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AtorResource REST controller.
 *
 * @see AtorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LocadoraApp.class)
public class AtorResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    @Autowired
    private AtorRepository atorRepository;

    @Mock
    private AtorRepository atorRepositoryMock;

    @Autowired
    private AtorMapper atorMapper;

    @Mock
    private AtorService atorServiceMock;

    @Autowired
    private AtorService atorService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAtorMockMvc;

    private Ator ator;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AtorResource atorResource = new AtorResource(atorService);
        this.restAtorMockMvc = MockMvcBuilders.standaloneSetup(atorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ator createEntity(EntityManager em) {
        Ator ator = new Ator()
            .nome(DEFAULT_NOME);
        return ator;
    }

    @Before
    public void initTest() {
        ator = createEntity(em);
    }

    @Test
    @Transactional
    public void createAtor() throws Exception {
        int databaseSizeBeforeCreate = atorRepository.findAll().size();

        // Create the Ator
        AtorDTO atorDTO = atorMapper.toDto(ator);
        restAtorMockMvc.perform(post("/api/ators")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atorDTO)))
            .andExpect(status().isCreated());

        // Validate the Ator in the database
        List<Ator> atorList = atorRepository.findAll();
        assertThat(atorList).hasSize(databaseSizeBeforeCreate + 1);
        Ator testAtor = atorList.get(atorList.size() - 1);
        assertThat(testAtor.getNome()).isEqualTo(DEFAULT_NOME);
    }

    @Test
    @Transactional
    public void createAtorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = atorRepository.findAll().size();

        // Create the Ator with an existing ID
        ator.setId(1L);
        AtorDTO atorDTO = atorMapper.toDto(ator);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtorMockMvc.perform(post("/api/ators")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Ator in the database
        List<Ator> atorList = atorRepository.findAll();
        assertThat(atorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = atorRepository.findAll().size();
        // set the field null
        ator.setNome(null);

        // Create the Ator, which fails.
        AtorDTO atorDTO = atorMapper.toDto(ator);

        restAtorMockMvc.perform(post("/api/ators")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atorDTO)))
            .andExpect(status().isBadRequest());

        List<Ator> atorList = atorRepository.findAll();
        assertThat(atorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAtors() throws Exception {
        // Initialize the database
        atorRepository.saveAndFlush(ator);

        // Get all the atorList
        restAtorMockMvc.perform(get("/api/ators?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ator.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllAtorsWithEagerRelationshipsIsEnabled() throws Exception {
        AtorResource atorResource = new AtorResource(atorServiceMock);
        when(atorServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restAtorMockMvc = MockMvcBuilders.standaloneSetup(atorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAtorMockMvc.perform(get("/api/ators?eagerload=true"))
        .andExpect(status().isOk());

        verify(atorServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllAtorsWithEagerRelationshipsIsNotEnabled() throws Exception {
        AtorResource atorResource = new AtorResource(atorServiceMock);
            when(atorServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restAtorMockMvc = MockMvcBuilders.standaloneSetup(atorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAtorMockMvc.perform(get("/api/ators?eagerload=true"))
        .andExpect(status().isOk());

            verify(atorServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getAtor() throws Exception {
        // Initialize the database
        atorRepository.saveAndFlush(ator);

        // Get the ator
        restAtorMockMvc.perform(get("/api/ators/{id}", ator.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ator.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAtor() throws Exception {
        // Get the ator
        restAtorMockMvc.perform(get("/api/ators/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAtor() throws Exception {
        // Initialize the database
        atorRepository.saveAndFlush(ator);

        int databaseSizeBeforeUpdate = atorRepository.findAll().size();

        // Update the ator
        Ator updatedAtor = atorRepository.findById(ator.getId()).get();
        // Disconnect from session so that the updates on updatedAtor are not directly saved in db
        em.detach(updatedAtor);
        updatedAtor
            .nome(UPDATED_NOME);
        AtorDTO atorDTO = atorMapper.toDto(updatedAtor);

        restAtorMockMvc.perform(put("/api/ators")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atorDTO)))
            .andExpect(status().isOk());

        // Validate the Ator in the database
        List<Ator> atorList = atorRepository.findAll();
        assertThat(atorList).hasSize(databaseSizeBeforeUpdate);
        Ator testAtor = atorList.get(atorList.size() - 1);
        assertThat(testAtor.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    @Transactional
    public void updateNonExistingAtor() throws Exception {
        int databaseSizeBeforeUpdate = atorRepository.findAll().size();

        // Create the Ator
        AtorDTO atorDTO = atorMapper.toDto(ator);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtorMockMvc.perform(put("/api/ators")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Ator in the database
        List<Ator> atorList = atorRepository.findAll();
        assertThat(atorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAtor() throws Exception {
        // Initialize the database
        atorRepository.saveAndFlush(ator);

        int databaseSizeBeforeDelete = atorRepository.findAll().size();

        // Delete the ator
        restAtorMockMvc.perform(delete("/api/ators/{id}", ator.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Ator> atorList = atorRepository.findAll();
        assertThat(atorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Ator.class);
        Ator ator1 = new Ator();
        ator1.setId(1L);
        Ator ator2 = new Ator();
        ator2.setId(ator1.getId());
        assertThat(ator1).isEqualTo(ator2);
        ator2.setId(2L);
        assertThat(ator1).isNotEqualTo(ator2);
        ator1.setId(null);
        assertThat(ator1).isNotEqualTo(ator2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AtorDTO.class);
        AtorDTO atorDTO1 = new AtorDTO();
        atorDTO1.setId(1L);
        AtorDTO atorDTO2 = new AtorDTO();
        assertThat(atorDTO1).isNotEqualTo(atorDTO2);
        atorDTO2.setId(atorDTO1.getId());
        assertThat(atorDTO1).isEqualTo(atorDTO2);
        atorDTO2.setId(2L);
        assertThat(atorDTO1).isNotEqualTo(atorDTO2);
        atorDTO1.setId(null);
        assertThat(atorDTO1).isNotEqualTo(atorDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(atorMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(atorMapper.fromId(null)).isNull();
    }
}
