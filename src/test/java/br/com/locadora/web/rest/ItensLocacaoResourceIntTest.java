package br.com.locadora.web.rest;

import br.com.locadora.LocadoraApp;

import br.com.locadora.domain.ItensLocacao;
import br.com.locadora.domain.Midia;
import br.com.locadora.domain.Locacao;
import br.com.locadora.repository.ItensLocacaoRepository;
import br.com.locadora.service.ItensLocacaoService;
import br.com.locadora.service.dto.ItensLocacaoDTO;
import br.com.locadora.service.mapper.ItensLocacaoMapper;
import br.com.locadora.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static br.com.locadora.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ItensLocacaoResource REST controller.
 *
 * @see ItensLocacaoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LocadoraApp.class)
public class ItensLocacaoResourceIntTest {

    private static final LocalDate DEFAULT_DATA_ENTREGA_PREVISTA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_ENTREGA_PREVISTA = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATA_ENTREGA_EFETIVA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_ENTREGA_EFETIVA = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ItensLocacaoRepository itensLocacaoRepository;

    @Autowired
    private ItensLocacaoMapper itensLocacaoMapper;

    @Autowired
    private ItensLocacaoService itensLocacaoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restItensLocacaoMockMvc;

    private ItensLocacao itensLocacao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ItensLocacaoResource itensLocacaoResource = new ItensLocacaoResource(itensLocacaoService);
        this.restItensLocacaoMockMvc = MockMvcBuilders.standaloneSetup(itensLocacaoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ItensLocacao createEntity(EntityManager em) {
        ItensLocacao itensLocacao = new ItensLocacao()
            .dataEntregaPrevista(DEFAULT_DATA_ENTREGA_PREVISTA)
            .dataEntregaEfetiva(DEFAULT_DATA_ENTREGA_EFETIVA);
        // Add required entity
        Midia midia = MidiaResourceIntTest.createEntity(em);
        em.persist(midia);
        em.flush();
        itensLocacao.setMidia(midia);
        // Add required entity
        Locacao locacao = LocacaoResourceIntTest.createEntity(em);
        em.persist(locacao);
        em.flush();
        itensLocacao.setLocacao(locacao);
        return itensLocacao;
    }

    @Before
    public void initTest() {
        itensLocacao = createEntity(em);
    }

    @Test
    @Transactional
    public void createItensLocacao() throws Exception {
        int databaseSizeBeforeCreate = itensLocacaoRepository.findAll().size();

        // Create the ItensLocacao
        ItensLocacaoDTO itensLocacaoDTO = itensLocacaoMapper.toDto(itensLocacao);
        restItensLocacaoMockMvc.perform(post("/api/itens-locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itensLocacaoDTO)))
            .andExpect(status().isCreated());

        // Validate the ItensLocacao in the database
        List<ItensLocacao> itensLocacaoList = itensLocacaoRepository.findAll();
        assertThat(itensLocacaoList).hasSize(databaseSizeBeforeCreate + 1);
        ItensLocacao testItensLocacao = itensLocacaoList.get(itensLocacaoList.size() - 1);
        assertThat(testItensLocacao.getDataEntregaPrevista()).isEqualTo(DEFAULT_DATA_ENTREGA_PREVISTA);
        assertThat(testItensLocacao.getDataEntregaEfetiva()).isEqualTo(DEFAULT_DATA_ENTREGA_EFETIVA);
    }

    @Test
    @Transactional
    public void createItensLocacaoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = itensLocacaoRepository.findAll().size();

        // Create the ItensLocacao with an existing ID
        itensLocacao.setId(1L);
        ItensLocacaoDTO itensLocacaoDTO = itensLocacaoMapper.toDto(itensLocacao);

        // An entity with an existing ID cannot be created, so this API call must fail
        restItensLocacaoMockMvc.perform(post("/api/itens-locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itensLocacaoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ItensLocacao in the database
        List<ItensLocacao> itensLocacaoList = itensLocacaoRepository.findAll();
        assertThat(itensLocacaoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDataEntregaPrevistaIsRequired() throws Exception {
        int databaseSizeBeforeTest = itensLocacaoRepository.findAll().size();
        // set the field null
        itensLocacao.setDataEntregaPrevista(null);

        // Create the ItensLocacao, which fails.
        ItensLocacaoDTO itensLocacaoDTO = itensLocacaoMapper.toDto(itensLocacao);

        restItensLocacaoMockMvc.perform(post("/api/itens-locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itensLocacaoDTO)))
            .andExpect(status().isBadRequest());

        List<ItensLocacao> itensLocacaoList = itensLocacaoRepository.findAll();
        assertThat(itensLocacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllItensLocacaos() throws Exception {
        // Initialize the database
        itensLocacaoRepository.saveAndFlush(itensLocacao);

        // Get all the itensLocacaoList
        restItensLocacaoMockMvc.perform(get("/api/itens-locacaos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(itensLocacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataEntregaPrevista").value(hasItem(DEFAULT_DATA_ENTREGA_PREVISTA.toString())))
            .andExpect(jsonPath("$.[*].dataEntregaEfetiva").value(hasItem(DEFAULT_DATA_ENTREGA_EFETIVA.toString())));
    }
    
    @Test
    @Transactional
    public void getItensLocacao() throws Exception {
        // Initialize the database
        itensLocacaoRepository.saveAndFlush(itensLocacao);

        // Get the itensLocacao
        restItensLocacaoMockMvc.perform(get("/api/itens-locacaos/{id}", itensLocacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(itensLocacao.getId().intValue()))
            .andExpect(jsonPath("$.dataEntregaPrevista").value(DEFAULT_DATA_ENTREGA_PREVISTA.toString()))
            .andExpect(jsonPath("$.dataEntregaEfetiva").value(DEFAULT_DATA_ENTREGA_EFETIVA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingItensLocacao() throws Exception {
        // Get the itensLocacao
        restItensLocacaoMockMvc.perform(get("/api/itens-locacaos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateItensLocacao() throws Exception {
        // Initialize the database
        itensLocacaoRepository.saveAndFlush(itensLocacao);

        int databaseSizeBeforeUpdate = itensLocacaoRepository.findAll().size();

        // Update the itensLocacao
        ItensLocacao updatedItensLocacao = itensLocacaoRepository.findById(itensLocacao.getId()).get();
        // Disconnect from session so that the updates on updatedItensLocacao are not directly saved in db
        em.detach(updatedItensLocacao);
        updatedItensLocacao
            .dataEntregaPrevista(UPDATED_DATA_ENTREGA_PREVISTA)
            .dataEntregaEfetiva(UPDATED_DATA_ENTREGA_EFETIVA);
        ItensLocacaoDTO itensLocacaoDTO = itensLocacaoMapper.toDto(updatedItensLocacao);

        restItensLocacaoMockMvc.perform(put("/api/itens-locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itensLocacaoDTO)))
            .andExpect(status().isOk());

        // Validate the ItensLocacao in the database
        List<ItensLocacao> itensLocacaoList = itensLocacaoRepository.findAll();
        assertThat(itensLocacaoList).hasSize(databaseSizeBeforeUpdate);
        ItensLocacao testItensLocacao = itensLocacaoList.get(itensLocacaoList.size() - 1);
        assertThat(testItensLocacao.getDataEntregaPrevista()).isEqualTo(UPDATED_DATA_ENTREGA_PREVISTA);
        assertThat(testItensLocacao.getDataEntregaEfetiva()).isEqualTo(UPDATED_DATA_ENTREGA_EFETIVA);
    }

    @Test
    @Transactional
    public void updateNonExistingItensLocacao() throws Exception {
        int databaseSizeBeforeUpdate = itensLocacaoRepository.findAll().size();

        // Create the ItensLocacao
        ItensLocacaoDTO itensLocacaoDTO = itensLocacaoMapper.toDto(itensLocacao);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restItensLocacaoMockMvc.perform(put("/api/itens-locacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itensLocacaoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ItensLocacao in the database
        List<ItensLocacao> itensLocacaoList = itensLocacaoRepository.findAll();
        assertThat(itensLocacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteItensLocacao() throws Exception {
        // Initialize the database
        itensLocacaoRepository.saveAndFlush(itensLocacao);

        int databaseSizeBeforeDelete = itensLocacaoRepository.findAll().size();

        // Delete the itensLocacao
        restItensLocacaoMockMvc.perform(delete("/api/itens-locacaos/{id}", itensLocacao.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ItensLocacao> itensLocacaoList = itensLocacaoRepository.findAll();
        assertThat(itensLocacaoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ItensLocacao.class);
        ItensLocacao itensLocacao1 = new ItensLocacao();
        itensLocacao1.setId(1L);
        ItensLocacao itensLocacao2 = new ItensLocacao();
        itensLocacao2.setId(itensLocacao1.getId());
        assertThat(itensLocacao1).isEqualTo(itensLocacao2);
        itensLocacao2.setId(2L);
        assertThat(itensLocacao1).isNotEqualTo(itensLocacao2);
        itensLocacao1.setId(null);
        assertThat(itensLocacao1).isNotEqualTo(itensLocacao2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ItensLocacaoDTO.class);
        ItensLocacaoDTO itensLocacaoDTO1 = new ItensLocacaoDTO();
        itensLocacaoDTO1.setId(1L);
        ItensLocacaoDTO itensLocacaoDTO2 = new ItensLocacaoDTO();
        assertThat(itensLocacaoDTO1).isNotEqualTo(itensLocacaoDTO2);
        itensLocacaoDTO2.setId(itensLocacaoDTO1.getId());
        assertThat(itensLocacaoDTO1).isEqualTo(itensLocacaoDTO2);
        itensLocacaoDTO2.setId(2L);
        assertThat(itensLocacaoDTO1).isNotEqualTo(itensLocacaoDTO2);
        itensLocacaoDTO1.setId(null);
        assertThat(itensLocacaoDTO1).isNotEqualTo(itensLocacaoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(itensLocacaoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(itensLocacaoMapper.fromId(null)).isNull();
    }
}
