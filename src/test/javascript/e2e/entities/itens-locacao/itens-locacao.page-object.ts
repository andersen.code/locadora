import { element, by, ElementFinder } from 'protractor';

export class ItensLocacaoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-itens-locacao div table .btn-danger'));
    title = element.all(by.css('jhi-itens-locacao div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ItensLocacaoUpdatePage {
    pageTitle = element(by.id('jhi-itens-locacao-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    dataEntregaPrevistaInput = element(by.id('field_dataEntregaPrevista'));
    dataEntregaEfetivaInput = element(by.id('field_dataEntregaEfetiva'));
    midiaSelect = element(by.id('field_midia'));
    locacaoSelect = element(by.id('field_locacao'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setDataEntregaPrevistaInput(dataEntregaPrevista) {
        await this.dataEntregaPrevistaInput.sendKeys(dataEntregaPrevista);
    }

    async getDataEntregaPrevistaInput() {
        return this.dataEntregaPrevistaInput.getAttribute('value');
    }

    async setDataEntregaEfetivaInput(dataEntregaEfetiva) {
        await this.dataEntregaEfetivaInput.sendKeys(dataEntregaEfetiva);
    }

    async getDataEntregaEfetivaInput() {
        return this.dataEntregaEfetivaInput.getAttribute('value');
    }

    async midiaSelectLastOption() {
        await this.midiaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async midiaSelectOption(option) {
        await this.midiaSelect.sendKeys(option);
    }

    getMidiaSelect(): ElementFinder {
        return this.midiaSelect;
    }

    async getMidiaSelectedOption() {
        return this.midiaSelect.element(by.css('option:checked')).getText();
    }

    async locacaoSelectLastOption() {
        await this.locacaoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async locacaoSelectOption(option) {
        await this.locacaoSelect.sendKeys(option);
    }

    getLocacaoSelect(): ElementFinder {
        return this.locacaoSelect;
    }

    async getLocacaoSelectedOption() {
        return this.locacaoSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ItensLocacaoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-itensLocacao-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-itensLocacao'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
