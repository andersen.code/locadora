/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ItensLocacaoComponentsPage, ItensLocacaoDeleteDialog, ItensLocacaoUpdatePage } from './itens-locacao.page-object';

const expect = chai.expect;

describe('ItensLocacao e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let itensLocacaoUpdatePage: ItensLocacaoUpdatePage;
    let itensLocacaoComponentsPage: ItensLocacaoComponentsPage;
    /*let itensLocacaoDeleteDialog: ItensLocacaoDeleteDialog;*/

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load ItensLocacaos', async () => {
        await navBarPage.goToEntity('itens-locacao');
        itensLocacaoComponentsPage = new ItensLocacaoComponentsPage();
        await browser.wait(ec.visibilityOf(itensLocacaoComponentsPage.title), 5000);
        expect(await itensLocacaoComponentsPage.getTitle()).to.eq('locadoraApp.itensLocacao.home.title');
    });

    it('should load create ItensLocacao page', async () => {
        await itensLocacaoComponentsPage.clickOnCreateButton();
        itensLocacaoUpdatePage = new ItensLocacaoUpdatePage();
        expect(await itensLocacaoUpdatePage.getPageTitle()).to.eq('locadoraApp.itensLocacao.home.createOrEditLabel');
        await itensLocacaoUpdatePage.cancel();
    });

    /* it('should create and save ItensLocacaos', async () => {
        const nbButtonsBeforeCreate = await itensLocacaoComponentsPage.countDeleteButtons();

        await itensLocacaoComponentsPage.clickOnCreateButton();
        await promise.all([
            itensLocacaoUpdatePage.setDataEntregaPrevistaInput('2000-12-31'),
            itensLocacaoUpdatePage.setDataEntregaEfetivaInput('2000-12-31'),
            itensLocacaoUpdatePage.midiaSelectLastOption(),
            itensLocacaoUpdatePage.locacaoSelectLastOption(),
        ]);
        expect(await itensLocacaoUpdatePage.getDataEntregaPrevistaInput()).to.eq('2000-12-31');
        expect(await itensLocacaoUpdatePage.getDataEntregaEfetivaInput()).to.eq('2000-12-31');
        await itensLocacaoUpdatePage.save();
        expect(await itensLocacaoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await itensLocacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });*/

    /* it('should delete last ItensLocacao', async () => {
        const nbButtonsBeforeDelete = await itensLocacaoComponentsPage.countDeleteButtons();
        await itensLocacaoComponentsPage.clickOnLastDeleteButton();

        itensLocacaoDeleteDialog = new ItensLocacaoDeleteDialog();
        expect(await itensLocacaoDeleteDialog.getDialogTitle())
            .to.eq('locadoraApp.itensLocacao.delete.question');
        await itensLocacaoDeleteDialog.clickOnConfirmButton();

        expect(await itensLocacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
