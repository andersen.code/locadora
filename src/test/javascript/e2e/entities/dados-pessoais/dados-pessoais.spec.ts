/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DadosPessoaisComponentsPage, DadosPessoaisDeleteDialog, DadosPessoaisUpdatePage } from './dados-pessoais.page-object';

const expect = chai.expect;

describe('DadosPessoais e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let dadosPessoaisUpdatePage: DadosPessoaisUpdatePage;
    let dadosPessoaisComponentsPage: DadosPessoaisComponentsPage;
    /*let dadosPessoaisDeleteDialog: DadosPessoaisDeleteDialog;*/

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load DadosPessoais', async () => {
        await navBarPage.goToEntity('dados-pessoais');
        dadosPessoaisComponentsPage = new DadosPessoaisComponentsPage();
        await browser.wait(ec.visibilityOf(dadosPessoaisComponentsPage.title), 5000);
        expect(await dadosPessoaisComponentsPage.getTitle()).to.eq('locadoraApp.dadosPessoais.home.title');
    });

    it('should load create DadosPessoais page', async () => {
        await dadosPessoaisComponentsPage.clickOnCreateButton();
        dadosPessoaisUpdatePage = new DadosPessoaisUpdatePage();
        expect(await dadosPessoaisUpdatePage.getPageTitle()).to.eq('locadoraApp.dadosPessoais.home.createOrEditLabel');
        await dadosPessoaisUpdatePage.cancel();
    });

    /* it('should create and save DadosPessoais', async () => {
        const nbButtonsBeforeCreate = await dadosPessoaisComponentsPage.countDeleteButtons();

        await dadosPessoaisComponentsPage.clickOnCreateButton();
        await promise.all([
            dadosPessoaisUpdatePage.tipoPessoaSelectLastOption(),
            dadosPessoaisUpdatePage.setNomeInput('nome'),
            dadosPessoaisUpdatePage.setDocumentoReceitaFederalInput('documentoReceitaFederal'),
            dadosPessoaisUpdatePage.situacaoSelectLastOption(),
            dadosPessoaisUpdatePage.setObservacaoInput('observacao'),
            dadosPessoaisUpdatePage.userSelectLastOption(),
        ]);
        expect(await dadosPessoaisUpdatePage.getNomeInput()).to.eq('nome');
        expect(await dadosPessoaisUpdatePage.getDocumentoReceitaFederalInput()).to.eq('documentoReceitaFederal');
        expect(await dadosPessoaisUpdatePage.getObservacaoInput()).to.eq('observacao');
        await dadosPessoaisUpdatePage.save();
        expect(await dadosPessoaisUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await dadosPessoaisComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });*/

    /* it('should delete last DadosPessoais', async () => {
        const nbButtonsBeforeDelete = await dadosPessoaisComponentsPage.countDeleteButtons();
        await dadosPessoaisComponentsPage.clickOnLastDeleteButton();

        dadosPessoaisDeleteDialog = new DadosPessoaisDeleteDialog();
        expect(await dadosPessoaisDeleteDialog.getDialogTitle())
            .to.eq('locadoraApp.dadosPessoais.delete.question');
        await dadosPessoaisDeleteDialog.clickOnConfirmButton();

        expect(await dadosPessoaisComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
