import { element, by, ElementFinder } from 'protractor';

export class DadosPessoaisComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-dados-pessoais div table .btn-danger'));
    title = element.all(by.css('jhi-dados-pessoais div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class DadosPessoaisUpdatePage {
    pageTitle = element(by.id('jhi-dados-pessoais-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    tipoPessoaSelect = element(by.id('field_tipoPessoa'));
    nomeInput = element(by.id('field_nome'));
    documentoReceitaFederalInput = element(by.id('field_documentoReceitaFederal'));
    situacaoSelect = element(by.id('field_situacao'));
    observacaoInput = element(by.id('field_observacao'));
    userSelect = element(by.id('field_user'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setTipoPessoaSelect(tipoPessoa) {
        await this.tipoPessoaSelect.sendKeys(tipoPessoa);
    }

    async getTipoPessoaSelect() {
        return this.tipoPessoaSelect.element(by.css('option:checked')).getText();
    }

    async tipoPessoaSelectLastOption() {
        await this.tipoPessoaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setNomeInput(nome) {
        await this.nomeInput.sendKeys(nome);
    }

    async getNomeInput() {
        return this.nomeInput.getAttribute('value');
    }

    async setDocumentoReceitaFederalInput(documentoReceitaFederal) {
        await this.documentoReceitaFederalInput.sendKeys(documentoReceitaFederal);
    }

    async getDocumentoReceitaFederalInput() {
        return this.documentoReceitaFederalInput.getAttribute('value');
    }

    async setSituacaoSelect(situacao) {
        await this.situacaoSelect.sendKeys(situacao);
    }

    async getSituacaoSelect() {
        return this.situacaoSelect.element(by.css('option:checked')).getText();
    }

    async situacaoSelectLastOption() {
        await this.situacaoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setObservacaoInput(observacao) {
        await this.observacaoInput.sendKeys(observacao);
    }

    async getObservacaoInput() {
        return this.observacaoInput.getAttribute('value');
    }

    async userSelectLastOption() {
        await this.userSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async userSelectOption(option) {
        await this.userSelect.sendKeys(option);
    }

    getUserSelect(): ElementFinder {
        return this.userSelect;
    }

    async getUserSelectedOption() {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class DadosPessoaisDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-dadosPessoais-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-dadosPessoais'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
