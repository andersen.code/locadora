/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AtorComponentsPage, AtorDeleteDialog, AtorUpdatePage } from './ator.page-object';

const expect = chai.expect;

describe('Ator e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let atorUpdatePage: AtorUpdatePage;
    let atorComponentsPage: AtorComponentsPage;
    let atorDeleteDialog: AtorDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Ators', async () => {
        await navBarPage.goToEntity('ator');
        atorComponentsPage = new AtorComponentsPage();
        await browser.wait(ec.visibilityOf(atorComponentsPage.title), 5000);
        expect(await atorComponentsPage.getTitle()).to.eq('locadoraApp.ator.home.title');
    });

    it('should load create Ator page', async () => {
        await atorComponentsPage.clickOnCreateButton();
        atorUpdatePage = new AtorUpdatePage();
        expect(await atorUpdatePage.getPageTitle()).to.eq('locadoraApp.ator.home.createOrEditLabel');
        await atorUpdatePage.cancel();
    });

    it('should create and save Ators', async () => {
        const nbButtonsBeforeCreate = await atorComponentsPage.countDeleteButtons();

        await atorComponentsPage.clickOnCreateButton();
        await promise.all([
            atorUpdatePage.setNomeInput('nome')
            // atorUpdatePage.filmeSelectLastOption(),
        ]);
        expect(await atorUpdatePage.getNomeInput()).to.eq('nome');
        await atorUpdatePage.save();
        expect(await atorUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await atorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Ator', async () => {
        const nbButtonsBeforeDelete = await atorComponentsPage.countDeleteButtons();
        await atorComponentsPage.clickOnLastDeleteButton();

        atorDeleteDialog = new AtorDeleteDialog();
        expect(await atorDeleteDialog.getDialogTitle()).to.eq('locadoraApp.ator.delete.question');
        await atorDeleteDialog.clickOnConfirmButton();

        expect(await atorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
