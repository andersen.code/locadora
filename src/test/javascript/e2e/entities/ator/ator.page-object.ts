import { element, by, ElementFinder } from 'protractor';

export class AtorComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-ator div table .btn-danger'));
    title = element.all(by.css('jhi-ator div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AtorUpdatePage {
    pageTitle = element(by.id('jhi-ator-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nomeInput = element(by.id('field_nome'));
    filmeSelect = element(by.id('field_filme'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNomeInput(nome) {
        await this.nomeInput.sendKeys(nome);
    }

    async getNomeInput() {
        return this.nomeInput.getAttribute('value');
    }

    async filmeSelectLastOption() {
        await this.filmeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async filmeSelectOption(option) {
        await this.filmeSelect.sendKeys(option);
    }

    getFilmeSelect(): ElementFinder {
        return this.filmeSelect;
    }

    async getFilmeSelectedOption() {
        return this.filmeSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AtorDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-ator-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-ator'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
