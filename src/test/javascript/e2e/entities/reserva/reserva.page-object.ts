import { element, by, ElementFinder } from 'protractor';

export class ReservaComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-reserva div table .btn-danger'));
    title = element.all(by.css('jhi-reserva div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ReservaUpdatePage {
    pageTitle = element(by.id('jhi-reserva-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    dataLocacaoPrevistaInput = element(by.id('field_dataLocacaoPrevista'));
    dataValidadeReservaInput = element(by.id('field_dataValidadeReserva'));
    observacaoInput = element(by.id('field_observacao'));
    userSelect = element(by.id('field_user'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setDataLocacaoPrevistaInput(dataLocacaoPrevista) {
        await this.dataLocacaoPrevistaInput.sendKeys(dataLocacaoPrevista);
    }

    async getDataLocacaoPrevistaInput() {
        return this.dataLocacaoPrevistaInput.getAttribute('value');
    }

    async setDataValidadeReservaInput(dataValidadeReserva) {
        await this.dataValidadeReservaInput.sendKeys(dataValidadeReserva);
    }

    async getDataValidadeReservaInput() {
        return this.dataValidadeReservaInput.getAttribute('value');
    }

    async setObservacaoInput(observacao) {
        await this.observacaoInput.sendKeys(observacao);
    }

    async getObservacaoInput() {
        return this.observacaoInput.getAttribute('value');
    }

    async userSelectLastOption() {
        await this.userSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async userSelectOption(option) {
        await this.userSelect.sendKeys(option);
    }

    getUserSelect(): ElementFinder {
        return this.userSelect;
    }

    async getUserSelectedOption() {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ReservaDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-reserva-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-reserva'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
