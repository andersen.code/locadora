import { element, by, ElementFinder } from 'protractor';

export class MidiaComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-midia div table .btn-danger'));
    title = element.all(by.css('jhi-midia div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MidiaUpdatePage {
    pageTitle = element(by.id('jhi-midia-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idiomaSelect = element(by.id('field_idioma'));
    tipoSelect = element(by.id('field_tipo'));
    statusMidiaSelect = element(by.id('field_statusMidia'));
    filmeSelect = element(by.id('field_filme'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdiomaSelect(idioma) {
        await this.idiomaSelect.sendKeys(idioma);
    }

    async getIdiomaSelect() {
        return this.idiomaSelect.element(by.css('option:checked')).getText();
    }

    async idiomaSelectLastOption() {
        await this.idiomaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setTipoSelect(tipo) {
        await this.tipoSelect.sendKeys(tipo);
    }

    async getTipoSelect() {
        return this.tipoSelect.element(by.css('option:checked')).getText();
    }

    async tipoSelectLastOption() {
        await this.tipoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setStatusMidiaSelect(statusMidia) {
        await this.statusMidiaSelect.sendKeys(statusMidia);
    }

    async getStatusMidiaSelect() {
        return this.statusMidiaSelect.element(by.css('option:checked')).getText();
    }

    async statusMidiaSelectLastOption() {
        await this.statusMidiaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async filmeSelectLastOption() {
        await this.filmeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async filmeSelectOption(option) {
        await this.filmeSelect.sendKeys(option);
    }

    getFilmeSelect(): ElementFinder {
        return this.filmeSelect;
    }

    async getFilmeSelectedOption() {
        return this.filmeSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class MidiaDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-midia-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-midia'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
