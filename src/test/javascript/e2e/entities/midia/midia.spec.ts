/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MidiaComponentsPage, MidiaDeleteDialog, MidiaUpdatePage } from './midia.page-object';

const expect = chai.expect;

describe('Midia e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let midiaUpdatePage: MidiaUpdatePage;
    let midiaComponentsPage: MidiaComponentsPage;
    let midiaDeleteDialog: MidiaDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Midias', async () => {
        await navBarPage.goToEntity('midia');
        midiaComponentsPage = new MidiaComponentsPage();
        await browser.wait(ec.visibilityOf(midiaComponentsPage.title), 5000);
        expect(await midiaComponentsPage.getTitle()).to.eq('locadoraApp.midia.home.title');
    });

    it('should load create Midia page', async () => {
        await midiaComponentsPage.clickOnCreateButton();
        midiaUpdatePage = new MidiaUpdatePage();
        expect(await midiaUpdatePage.getPageTitle()).to.eq('locadoraApp.midia.home.createOrEditLabel');
        await midiaUpdatePage.cancel();
    });

    it('should create and save Midias', async () => {
        const nbButtonsBeforeCreate = await midiaComponentsPage.countDeleteButtons();

        await midiaComponentsPage.clickOnCreateButton();
        await promise.all([
            midiaUpdatePage.idiomaSelectLastOption(),
            midiaUpdatePage.tipoSelectLastOption(),
            midiaUpdatePage.statusMidiaSelectLastOption()
            // midiaUpdatePage.filmeSelectLastOption(),
        ]);
        await midiaUpdatePage.save();
        expect(await midiaUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await midiaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Midia', async () => {
        const nbButtonsBeforeDelete = await midiaComponentsPage.countDeleteButtons();
        await midiaComponentsPage.clickOnLastDeleteButton();

        midiaDeleteDialog = new MidiaDeleteDialog();
        expect(await midiaDeleteDialog.getDialogTitle()).to.eq('locadoraApp.midia.delete.question');
        await midiaDeleteDialog.clickOnConfirmButton();

        expect(await midiaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
