/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FilmeComponentsPage, FilmeDeleteDialog, FilmeUpdatePage } from './filme.page-object';

const expect = chai.expect;

describe('Filme e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let filmeUpdatePage: FilmeUpdatePage;
    let filmeComponentsPage: FilmeComponentsPage;
    let filmeDeleteDialog: FilmeDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Filmes', async () => {
        await navBarPage.goToEntity('filme');
        filmeComponentsPage = new FilmeComponentsPage();
        await browser.wait(ec.visibilityOf(filmeComponentsPage.title), 5000);
        expect(await filmeComponentsPage.getTitle()).to.eq('locadoraApp.filme.home.title');
    });

    it('should load create Filme page', async () => {
        await filmeComponentsPage.clickOnCreateButton();
        filmeUpdatePage = new FilmeUpdatePage();
        expect(await filmeUpdatePage.getPageTitle()).to.eq('locadoraApp.filme.home.createOrEditLabel');
        await filmeUpdatePage.cancel();
    });

    it('should create and save Filmes', async () => {
        const nbButtonsBeforeCreate = await filmeComponentsPage.countDeleteButtons();

        await filmeComponentsPage.clickOnCreateButton();
        await promise.all([
            filmeUpdatePage.setTituloInput('titulo'),
            filmeUpdatePage.categoriaSelectLastOption(),
            filmeUpdatePage.diretorSelectLastOption()
        ]);
        expect(await filmeUpdatePage.getTituloInput()).to.eq('titulo');
        await filmeUpdatePage.save();
        expect(await filmeUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await filmeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Filme', async () => {
        const nbButtonsBeforeDelete = await filmeComponentsPage.countDeleteButtons();
        await filmeComponentsPage.clickOnLastDeleteButton();

        filmeDeleteDialog = new FilmeDeleteDialog();
        expect(await filmeDeleteDialog.getDialogTitle()).to.eq('locadoraApp.filme.delete.question');
        await filmeDeleteDialog.clickOnConfirmButton();

        expect(await filmeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
