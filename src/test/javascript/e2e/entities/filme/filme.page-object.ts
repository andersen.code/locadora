import { element, by, ElementFinder } from 'protractor';

export class FilmeComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-filme div table .btn-danger'));
    title = element.all(by.css('jhi-filme div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class FilmeUpdatePage {
    pageTitle = element(by.id('jhi-filme-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    tituloInput = element(by.id('field_titulo'));
    categoriaSelect = element(by.id('field_categoria'));
    diretorSelect = element(by.id('field_diretor'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setTituloInput(titulo) {
        await this.tituloInput.sendKeys(titulo);
    }

    async getTituloInput() {
        return this.tituloInput.getAttribute('value');
    }

    async setCategoriaSelect(categoria) {
        await this.categoriaSelect.sendKeys(categoria);
    }

    async getCategoriaSelect() {
        return this.categoriaSelect.element(by.css('option:checked')).getText();
    }

    async categoriaSelectLastOption() {
        await this.categoriaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async diretorSelectLastOption() {
        await this.diretorSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async diretorSelectOption(option) {
        await this.diretorSelect.sendKeys(option);
    }

    getDiretorSelect(): ElementFinder {
        return this.diretorSelect;
    }

    async getDiretorSelectedOption() {
        return this.diretorSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class FilmeDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-filme-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-filme'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
