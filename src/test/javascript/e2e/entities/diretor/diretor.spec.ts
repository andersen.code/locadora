/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DiretorComponentsPage, DiretorDeleteDialog, DiretorUpdatePage } from './diretor.page-object';

const expect = chai.expect;

describe('Diretor e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let diretorUpdatePage: DiretorUpdatePage;
    let diretorComponentsPage: DiretorComponentsPage;
    let diretorDeleteDialog: DiretorDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Diretors', async () => {
        await navBarPage.goToEntity('diretor');
        diretorComponentsPage = new DiretorComponentsPage();
        await browser.wait(ec.visibilityOf(diretorComponentsPage.title), 5000);
        expect(await diretorComponentsPage.getTitle()).to.eq('locadoraApp.diretor.home.title');
    });

    it('should load create Diretor page', async () => {
        await diretorComponentsPage.clickOnCreateButton();
        diretorUpdatePage = new DiretorUpdatePage();
        expect(await diretorUpdatePage.getPageTitle()).to.eq('locadoraApp.diretor.home.createOrEditLabel');
        await diretorUpdatePage.cancel();
    });

    it('should create and save Diretors', async () => {
        const nbButtonsBeforeCreate = await diretorComponentsPage.countDeleteButtons();

        await diretorComponentsPage.clickOnCreateButton();
        await promise.all([diretorUpdatePage.setNomeInput('nome'), diretorUpdatePage.setObservacaoInput('observacao')]);
        expect(await diretorUpdatePage.getNomeInput()).to.eq('nome');
        expect(await diretorUpdatePage.getObservacaoInput()).to.eq('observacao');
        await diretorUpdatePage.save();
        expect(await diretorUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await diretorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Diretor', async () => {
        const nbButtonsBeforeDelete = await diretorComponentsPage.countDeleteButtons();
        await diretorComponentsPage.clickOnLastDeleteButton();

        diretorDeleteDialog = new DiretorDeleteDialog();
        expect(await diretorDeleteDialog.getDialogTitle()).to.eq('locadoraApp.diretor.delete.question');
        await diretorDeleteDialog.clickOnConfirmButton();

        expect(await diretorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
