import { element, by, ElementFinder } from 'protractor';

export class LocacaoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-locacao div table .btn-danger'));
    title = element.all(by.css('jhi-locacao div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class LocacaoUpdatePage {
    pageTitle = element(by.id('jhi-locacao-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    dataLocacaoInput = element(by.id('field_dataLocacao'));
    valorLocacaoInput = element(by.id('field_valorLocacao'));
    observacaoInput = element(by.id('field_observacao'));
    userSelect = element(by.id('field_user'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setDataLocacaoInput(dataLocacao) {
        await this.dataLocacaoInput.sendKeys(dataLocacao);
    }

    async getDataLocacaoInput() {
        return this.dataLocacaoInput.getAttribute('value');
    }

    async setValorLocacaoInput(valorLocacao) {
        await this.valorLocacaoInput.sendKeys(valorLocacao);
    }

    async getValorLocacaoInput() {
        return this.valorLocacaoInput.getAttribute('value');
    }

    async setObservacaoInput(observacao) {
        await this.observacaoInput.sendKeys(observacao);
    }

    async getObservacaoInput() {
        return this.observacaoInput.getAttribute('value');
    }

    async userSelectLastOption() {
        await this.userSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async userSelectOption(option) {
        await this.userSelect.sendKeys(option);
    }

    getUserSelect(): ElementFinder {
        return this.userSelect;
    }

    async getUserSelectedOption() {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class LocacaoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-locacao-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-locacao'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
