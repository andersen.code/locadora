/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { LocacaoComponentsPage, LocacaoDeleteDialog, LocacaoUpdatePage } from './locacao.page-object';

const expect = chai.expect;

describe('Locacao e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let locacaoUpdatePage: LocacaoUpdatePage;
    let locacaoComponentsPage: LocacaoComponentsPage;
    let locacaoDeleteDialog: LocacaoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Locacaos', async () => {
        await navBarPage.goToEntity('locacao');
        locacaoComponentsPage = new LocacaoComponentsPage();
        await browser.wait(ec.visibilityOf(locacaoComponentsPage.title), 5000);
        expect(await locacaoComponentsPage.getTitle()).to.eq('locadoraApp.locacao.home.title');
    });

    it('should load create Locacao page', async () => {
        await locacaoComponentsPage.clickOnCreateButton();
        locacaoUpdatePage = new LocacaoUpdatePage();
        expect(await locacaoUpdatePage.getPageTitle()).to.eq('locadoraApp.locacao.home.createOrEditLabel');
        await locacaoUpdatePage.cancel();
    });

    it('should create and save Locacaos', async () => {
        const nbButtonsBeforeCreate = await locacaoComponentsPage.countDeleteButtons();

        await locacaoComponentsPage.clickOnCreateButton();
        await promise.all([
            locacaoUpdatePage.setDataLocacaoInput('2000-12-31'),
            locacaoUpdatePage.setValorLocacaoInput('5'),
            locacaoUpdatePage.setObservacaoInput('observacao'),
            locacaoUpdatePage.userSelectLastOption()
        ]);
        expect(await locacaoUpdatePage.getDataLocacaoInput()).to.eq('2000-12-31');
        expect(await locacaoUpdatePage.getValorLocacaoInput()).to.eq('5');
        expect(await locacaoUpdatePage.getObservacaoInput()).to.eq('observacao');
        await locacaoUpdatePage.save();
        expect(await locacaoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await locacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Locacao', async () => {
        const nbButtonsBeforeDelete = await locacaoComponentsPage.countDeleteButtons();
        await locacaoComponentsPage.clickOnLastDeleteButton();

        locacaoDeleteDialog = new LocacaoDeleteDialog();
        expect(await locacaoDeleteDialog.getDialogTitle()).to.eq('locadoraApp.locacao.delete.question');
        await locacaoDeleteDialog.clickOnConfirmButton();

        expect(await locacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
