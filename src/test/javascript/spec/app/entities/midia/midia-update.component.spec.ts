/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { MidiaUpdateComponent } from 'app/entities/midia/midia-update.component';
import { MidiaService } from 'app/entities/midia/midia.service';
import { Midia } from 'app/shared/model/midia.model';

describe('Component Tests', () => {
    describe('Midia Management Update Component', () => {
        let comp: MidiaUpdateComponent;
        let fixture: ComponentFixture<MidiaUpdateComponent>;
        let service: MidiaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [MidiaUpdateComponent]
            })
                .overrideTemplate(MidiaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(MidiaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MidiaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Midia(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.midia = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Midia();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.midia = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
