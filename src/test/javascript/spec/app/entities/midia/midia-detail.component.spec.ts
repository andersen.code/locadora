/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { MidiaDetailComponent } from 'app/entities/midia/midia-detail.component';
import { Midia } from 'app/shared/model/midia.model';

describe('Component Tests', () => {
    describe('Midia Management Detail Component', () => {
        let comp: MidiaDetailComponent;
        let fixture: ComponentFixture<MidiaDetailComponent>;
        const route = ({ data: of({ midia: new Midia(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [MidiaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(MidiaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(MidiaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.midia).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
