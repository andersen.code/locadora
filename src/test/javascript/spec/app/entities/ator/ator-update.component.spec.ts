/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { AtorUpdateComponent } from 'app/entities/ator/ator-update.component';
import { AtorService } from 'app/entities/ator/ator.service';
import { Ator } from 'app/shared/model/ator.model';

describe('Component Tests', () => {
    describe('Ator Management Update Component', () => {
        let comp: AtorUpdateComponent;
        let fixture: ComponentFixture<AtorUpdateComponent>;
        let service: AtorService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [AtorUpdateComponent]
            })
                .overrideTemplate(AtorUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AtorUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AtorService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Ator(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.ator = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Ator();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.ator = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
