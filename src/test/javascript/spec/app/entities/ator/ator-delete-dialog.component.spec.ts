/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { LocadoraTestModule } from '../../../test.module';
import { AtorDeleteDialogComponent } from 'app/entities/ator/ator-delete-dialog.component';
import { AtorService } from 'app/entities/ator/ator.service';

describe('Component Tests', () => {
    describe('Ator Management Delete Component', () => {
        let comp: AtorDeleteDialogComponent;
        let fixture: ComponentFixture<AtorDeleteDialogComponent>;
        let service: AtorService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [AtorDeleteDialogComponent]
            })
                .overrideTemplate(AtorDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AtorDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AtorService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
