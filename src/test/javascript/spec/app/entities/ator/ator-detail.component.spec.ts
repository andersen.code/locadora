/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { AtorDetailComponent } from 'app/entities/ator/ator-detail.component';
import { Ator } from 'app/shared/model/ator.model';

describe('Component Tests', () => {
    describe('Ator Management Detail Component', () => {
        let comp: AtorDetailComponent;
        let fixture: ComponentFixture<AtorDetailComponent>;
        const route = ({ data: of({ ator: new Ator(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [AtorDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AtorDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AtorDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.ator).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
