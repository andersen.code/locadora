/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { DadosPessoaisUpdateComponent } from 'app/entities/dados-pessoais/dados-pessoais-update.component';
import { DadosPessoaisService } from 'app/entities/dados-pessoais/dados-pessoais.service';
import { DadosPessoais } from 'app/shared/model/dados-pessoais.model';

describe('Component Tests', () => {
    describe('DadosPessoais Management Update Component', () => {
        let comp: DadosPessoaisUpdateComponent;
        let fixture: ComponentFixture<DadosPessoaisUpdateComponent>;
        let service: DadosPessoaisService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [DadosPessoaisUpdateComponent]
            })
                .overrideTemplate(DadosPessoaisUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DadosPessoaisUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DadosPessoaisService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new DadosPessoais(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.dadosPessoais = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new DadosPessoais();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.dadosPessoais = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
