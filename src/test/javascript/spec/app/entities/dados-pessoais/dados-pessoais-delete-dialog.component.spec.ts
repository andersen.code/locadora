/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { LocadoraTestModule } from '../../../test.module';
import { DadosPessoaisDeleteDialogComponent } from 'app/entities/dados-pessoais/dados-pessoais-delete-dialog.component';
import { DadosPessoaisService } from 'app/entities/dados-pessoais/dados-pessoais.service';

describe('Component Tests', () => {
    describe('DadosPessoais Management Delete Component', () => {
        let comp: DadosPessoaisDeleteDialogComponent;
        let fixture: ComponentFixture<DadosPessoaisDeleteDialogComponent>;
        let service: DadosPessoaisService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [DadosPessoaisDeleteDialogComponent]
            })
                .overrideTemplate(DadosPessoaisDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DadosPessoaisDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DadosPessoaisService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
