/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { DadosPessoaisService } from 'app/entities/dados-pessoais/dados-pessoais.service';
import { IDadosPessoais, DadosPessoais, TipoPessoa, ClienteSituacao } from 'app/shared/model/dados-pessoais.model';

describe('Service Tests', () => {
    describe('DadosPessoais Service', () => {
        let injector: TestBed;
        let service: DadosPessoaisService;
        let httpMock: HttpTestingController;
        let elemDefault: IDadosPessoais;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(DadosPessoaisService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new DadosPessoais(0, TipoPessoa.FISICA, 'AAAAAAA', 'AAAAAAA', ClienteSituacao.ATIVO, 'AAAAAAA');
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a DadosPessoais', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new DadosPessoais(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a DadosPessoais', async () => {
                const returnedFromService = Object.assign(
                    {
                        tipoPessoa: 'BBBBBB',
                        nome: 'BBBBBB',
                        documentoReceitaFederal: 'BBBBBB',
                        situacao: 'BBBBBB',
                        observacao: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of DadosPessoais', async () => {
                const returnedFromService = Object.assign(
                    {
                        tipoPessoa: 'BBBBBB',
                        nome: 'BBBBBB',
                        documentoReceitaFederal: 'BBBBBB',
                        situacao: 'BBBBBB',
                        observacao: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a DadosPessoais', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
