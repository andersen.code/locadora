/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { DadosPessoaisDetailComponent } from 'app/entities/dados-pessoais/dados-pessoais-detail.component';
import { DadosPessoais } from 'app/shared/model/dados-pessoais.model';

describe('Component Tests', () => {
    describe('DadosPessoais Management Detail Component', () => {
        let comp: DadosPessoaisDetailComponent;
        let fixture: ComponentFixture<DadosPessoaisDetailComponent>;
        const route = ({ data: of({ dadosPessoais: new DadosPessoais(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [DadosPessoaisDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DadosPessoaisDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DadosPessoaisDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.dadosPessoais).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
