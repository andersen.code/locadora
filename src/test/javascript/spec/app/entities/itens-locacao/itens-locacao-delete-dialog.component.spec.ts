/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { LocadoraTestModule } from '../../../test.module';
import { ItensLocacaoDeleteDialogComponent } from 'app/entities/itens-locacao/itens-locacao-delete-dialog.component';
import { ItensLocacaoService } from 'app/entities/itens-locacao/itens-locacao.service';

describe('Component Tests', () => {
    describe('ItensLocacao Management Delete Component', () => {
        let comp: ItensLocacaoDeleteDialogComponent;
        let fixture: ComponentFixture<ItensLocacaoDeleteDialogComponent>;
        let service: ItensLocacaoService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [ItensLocacaoDeleteDialogComponent]
            })
                .overrideTemplate(ItensLocacaoDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ItensLocacaoDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ItensLocacaoService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
