/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { ItensLocacaoDetailComponent } from 'app/entities/itens-locacao/itens-locacao-detail.component';
import { ItensLocacao } from 'app/shared/model/itens-locacao.model';

describe('Component Tests', () => {
    describe('ItensLocacao Management Detail Component', () => {
        let comp: ItensLocacaoDetailComponent;
        let fixture: ComponentFixture<ItensLocacaoDetailComponent>;
        const route = ({ data: of({ itensLocacao: new ItensLocacao(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [ItensLocacaoDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ItensLocacaoDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ItensLocacaoDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.itensLocacao).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
