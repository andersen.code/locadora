/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { LocadoraTestModule } from '../../../test.module';
import { ItensLocacaoUpdateComponent } from 'app/entities/itens-locacao/itens-locacao-update.component';
import { ItensLocacaoService } from 'app/entities/itens-locacao/itens-locacao.service';
import { ItensLocacao } from 'app/shared/model/itens-locacao.model';

describe('Component Tests', () => {
    describe('ItensLocacao Management Update Component', () => {
        let comp: ItensLocacaoUpdateComponent;
        let fixture: ComponentFixture<ItensLocacaoUpdateComponent>;
        let service: ItensLocacaoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [ItensLocacaoUpdateComponent]
            })
                .overrideTemplate(ItensLocacaoUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ItensLocacaoUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ItensLocacaoService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new ItensLocacao(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.itensLocacao = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new ItensLocacao();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.itensLocacao = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
