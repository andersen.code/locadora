/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { LocadoraTestModule } from '../../../test.module';
import { DiretorDeleteDialogComponent } from 'app/entities/diretor/diretor-delete-dialog.component';
import { DiretorService } from 'app/entities/diretor/diretor.service';

describe('Component Tests', () => {
    describe('Diretor Management Delete Component', () => {
        let comp: DiretorDeleteDialogComponent;
        let fixture: ComponentFixture<DiretorDeleteDialogComponent>;
        let service: DiretorService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [LocadoraTestModule],
                declarations: [DiretorDeleteDialogComponent]
            })
                .overrideTemplate(DiretorDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DiretorDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DiretorService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
